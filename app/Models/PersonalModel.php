<?php
namespace App\Models;
use CodeIgniter\Model;

/**
 *
 */
class PersonalModel extends Model
{


public function stateFetch()
{
  $db = \Config\Database::connect();
  $query   = $db->query('SELECT state_id,state_names FROM state_master');
  return $query->getResultArray();
}


public function getUserData($student_id)
{
  $db = \Config\Database::connect();
  $builder = $db->table('student');
  $builder->where('student_id', $student_id);
  $query   = $builder->get();
  return $query->getResultArray();
}

public function getSubject()
{
  $db = \Config\Database::connect();
  $query   = $db->query('SELECT subject_id,subject_name FROM subjects_master');
  return $query->getResultArray();
}


function extraActivity(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('student_activity');
  $builder->insert($data);
}

public function awardInsert(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('awards_details');
  $builder->insert($data);
}

public function certificateInsert(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('certification_details');
  $builder->insert($data);
}


public function projectInsert(array $data)
{

  $db = \Config\Database::connect();
  $builder = $db->table('project_details');
  $builder->insert($data);

}

public function workExperienceInsert(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('work_expriencecs');
  $builder->insert($data);
}
public function getExtraActivities()
{
  $db = \Config\Database::connect();
  $query   = $db->query('SELECT activity_id,activity_name FROM activity_master');
  return $query->getResultArray();
}

function getCity($id = ""){
    $db = \Config\Database::connect();
    if(!empty($id)){
        $query = $this->db->order_by('name', 'ASC')->get_where('city_master', array('state_id' => $id));
        return $query->result_array();
    }else{
        // $query = $this->db->order_by('name', 'ASC')->get('city_master');
        // return $query->result_array();
    }
}

public function personalRegister(array $data)
{

$db = \Config\Database::connect();
$builder = $db->table('student');
$builder->insert($data);
$id=$db->insertID();
return $id;
  // protected $table = 'student';
  // protected $allowedFields=['first_name', 'last_name','email_id','password'	,'state_id',	'city_id', 'phone_num',	'parent_email',	'insta_handler',	'website_link'];
}

public function uploadImage(array $data)
{

  $db = \Config\Database::connect();
  $builder = $db->table('career_goal');
  $id=$builder->insert($data);

}

public function subject(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('student_subject');
  $builder->insert($data);
}

public function highSchool(array $data)
{
  $db = \Config\Database::connect();
  $builder = $db->table('student_high_school');
  $builder->insert($data);
}

protected $table = 'student';
protected $beforeInsert=['beforeInsert'];
protected $beforeUpdate=['beforeUpdate'];

protected function beforeInsert(array  $data)
{
$data=$this->passwordHash($data);
return $data;
}

protected function beforeUpdate(array  $data)
{
$data=$this->passwordHash($data);
return $data;

}

protected function passwordHash(array $data)
{

if (isset($data['data']['password'])) {
  $data['data']['password']=password_hash($data['data']['password'],PASSWORD_DEFAULT);
  return $data;
}


}

}




 ?>
