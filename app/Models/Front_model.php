<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Front_model extends CI_Model{
    /*
     * Get posts
     */
    function getskills(){
       
            $query = $this->db->get('skills');
            return $query->result();
         
    }

     function getCompanies($id = ""){
        if(!empty($id)){
            $query = $this->db->get_where('company', array('id' => $id));
            return $query->row_array();
        }else{
            $query = $this->db->get('company');
            return $query->result_array();
        }
    }

    function company_profile($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT c.*,(select name from tbl_state where id= c.state) as state_name,(select name from tbl_city where id= c.city) as city_name from  `company` as c where `id`= '".$id."'");
            return $query->result_array();
        }
    }

     function get_logo($id = ""){
       $this->db->select('logo');
       $this->db->where('id',$id);
       $query = $this->db->get('company');
       return $query->row();

    }

    function check_job($id = "",$job_id){
       $this->db->select('*');
       $this->db->where('user_id',$id);
       $this->db->where('job_id',$job_id);
       $query = $this->db->get('tbl_job_apply');
       //echo $this->db->last_query();
       return $query->row();

    }
   
    //---------
    function getCity($id = ""){
        if(!empty($id)){
            $query = $this->db->order_by('name', 'ASC')->get_where('tbl_city', array('state_id' => $id));
            return $query->result_array();
        }else{
            $query = $this->db->order_by('name', 'ASC')->get('tbl_city');
            return $query->result_array();
        }
    }

    //---------
    function getTblSchool($id = ""){
        if(!empty($id)){
            $query = $this->db->order_by('name', 'ASC')->get_where('tbl_schools', array('city_id' => $id));
            return $query->result_array();
        }else{
            $query = $this->db->order_by('name', 'ASC')->get('tbl_schools');
            return $query->result_array();
        }
    }
    
    function user_profile($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT st.*,(select school_name from schools where id = st.school_name) as school_name_display FROM `students` as st WHERE st.`id`= '".$id."'");
            return $query->result_array();
        }
    }

 function getSchool($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT *,(select name from tbl_city where id= s.city_id) as city_name FROM `schools` as s");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT *,(select name from tbl_city where id= s.city_id) as city_name FROM `schools` as s");
            return $query->result_array();
        }
    }


    function getFun($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT * FROM `fun_area` where id='".$id."'");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT * FROM `fun_area` ");
            return $query->result_array();
        }
    }



    function getSkill($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT * FROM `skills` ORDER BY `name` ASC where id='".$id."'");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT * FROM `skills` ORDER BY `name` ASC ");
            return $query->result_array();
        }
    }

    function getElective($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT * FROM `electives` where id='".$id."'");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT * FROM `electives` ");
            return $query->result_array();
        }
    }

     function getSub_Elective($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT se.*,e.name as elective FROM `sub_electives` as se,electives as e where se.elective_id = e.id and se.id='".$id."'");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT se.*,e.name as elective FROM `sub_electives` as se,electives as e where se.elective_id = e.id ");
            return $query->result_array();
        }
    }

     function getJobs($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT * FROM `jobs` where id='".$id."'");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT * FROM `jobs` ");
            return $query->result_array();
        }
    }

     function getIndustry($id = ""){
        if(!empty($id)){
            $query = $this->db->query("SELECT * FROM `industry` where id='".$id."' ");
            return $query->result_array();
        }else{
            $query = $this->db->query("SELECT * FROM `industry` ");
            return $query->result_array();
        }
    }

    /**
        ===========================================================
        Operation   :   Fetch States
                    -----------------------------------------------
        Input       :   id
                    -----------------------------------------------
        Return      :   
        ===========================================================
    **/


    //-----------
    function getStates($id = ""){
        if(!empty($id)){
           // $query = $this->db->get_where('tbl_state', array('id' => $id));
            $query = $this->db->query("SELECT s.name, count(distinct c.name) as citycount,s.id
FROM tbl_state s,tbl_city c
WHERE c.state_id = s.id   
GROUP BY s.name");
            return $query->row_array();
        }else{
            $query = $this->db->query("SELECT s.name, count(distinct c.name) as citycount,s.id
FROM tbl_state s,tbl_city c
WHERE c.state_id = s.id   
GROUP BY s.name
ORDER BY s.name ASC");
            return $query->result_array();
        }
    }


    /**
        ===========================================================
        Operation   :   Fetch student subjects
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getStudentSubjectModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_subject');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Fetch student Project
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getStudentProjectModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_project');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Fetch student Awards
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getStudentAwardModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_award');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Fetch student Certificate
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getStudentCertModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_cert');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Fetch student Work Experience
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getStudentWorkModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_work_exp');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Fetch subjects
                    -----------------------------------------------
        Input       :   
                    -----------------------------------------------
        Return      :   data in array form.
        ===========================================================
    **/

    function getSubjectModel(){
        $this->db->select('*');
        $this->db->from('high_school_subject');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result_array();
    }

    /**
        ===========================================================
        Operation   :   Get student Skill
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   Student skills in array form.
        ===========================================================
    **/

    function getStudentSkillModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_skill');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Get Extra Activities
                    -----------------------------------------------
        Input       :   
                    -----------------------------------------------
        Return      :   Extra Activities in array form.
        ===========================================================
    **/

    function getExtraActivitiesModel(){
        $this->db->select('*');
        $this->db->from('extra_activities');
        $this->db->order_by('name', 'ASC');
        return $this->db->get()->result_array();
    }


    /**
        ===========================================================
        Operation   :   Get Student Extra Activities
                    -----------------------------------------------
        Input       :   student id
                    -----------------------------------------------
        Return      :   Student Extra Activities in array form.
        ===========================================================
    **/

    function studentExtraActivitiesModel( $student_id ){
        $this->db->select('*');
        $this->db->from('student_extra_activities');
        $this->db->where('student_id', $student_id);
        return $this->db->get()->result_array();
    }


    /*
     * Insert post
     */
    public function insert($data = array()) {
        $insert = $this->db->insert('posts', $data);
        if($insert){
            return $this->db->insert_id();
        }else{
            return false;
        }
    }
    
    /*
     * Update post
     */
    public function update($data, $id) {
        if(!empty($data) && !empty($id)){
            $update = $this->db->update('posts', $data, array('id'=>$id));
            return $update?true:false;
        }else{
            return false;
        }
    }
    
    /*
     * Delete post
     */
    public function delete($id){
        $delete = $this->db->delete('posts',array('id'=>$id));
        return $delete?true:false;
    }

    public function update_status($tbl,$id,$status){
      $data['status'] = $status;
      $this->db->where('id', $id);
      $this->db->update($tbl,$data);
    }

/**
 * This function used to decrypt data
 * @param {string} $subscription_id : This is encrypt text
 */

    function decryptdata( $sData )

    {

        $url_id=base64_decode($sData);
        $id=(double)$url_id/6752415;
        return $id;

    }


/**
 * This function used to encrypt ata
 * @param {string} $id : This is plain id
 */

    function encryptdata( $sData )
    {

        $id=(double)$sData*6752415;
        return base64_encode($id);

    }

/**
 * This function used to get my current subscription plan
 * @param {string} $id : This is plain id
 */

    function getMySubscriptionModel( $user_id )
    {

        $this->db->select('student_subscription.*, subscription_master.name as subs_name, subscription_master.type as subs_type');
        $this->db->from('student_subscription');
        $this->db->join('subscription_master', 'student_subscription.subscription_id=subscription_master.subscription_id');
        $this->db->where('student_id', $user_id);
        return $this->db->get()->result_array();

    }

/**
 * This function used to get plan feature subscription plan
 */

    function getPlanFeatureModel()
    {

        $this->db->select('*');
        $this->db->from('plan_feature_master');
        $this->db->where('status', '1');
        $this->db->order_by('feature_order', 'ASC');
        return $this->db->get()->result_array();

    }

/**
 * This function used to get plans
 */

    function getPlansModel()
    {

        $this->db->select('*');
        $this->db->from('subscription_master');
        $this->db->where('status', '1');
        $this->db->order_by('subscription_master.display_order', 'ASC');
        $sub = $this->db->get()->result_array();

        foreach ($sub as &$key) {
            $key['encid'] = $this->encryptdata( $key['subscription_id'] );

            $this->db->select('*');
            $this->db->from('feature_subs_map');
            $this->db->join('plan_feature_master', 'feature_subs_map.feature_id=plan_feature_master.plan_feature_master_id');
            $this->db->where('subs_id', $key['subscription_id']);
            $this->db->order_by('plan_feature_master.feature_order', 'ASC');
            $feature = $this->db->get()->result_array();

            $key['feature'] = $feature;
        }
        return $sub;
    }


/**
 * This function used to get internship charge point
 */

    function getChargePointModel( $name="" )
    {

        $this->db->select('*');
        $this->db->from('charge_points');
        if( $name != "" ){
            $this->db->where("name", $name);
            return $this->db->get()->result_array();
        }else{
            return $this->db->get()->result_array();    
        }
        

    }

/**
 * This function used to insert payment details before payment
 * @param {integer} $insert_id : This is insert_id
 */

    function beforePaymentInsertModel( $data )
    {
        $this->db->insert('student_subs_payment', $data);
        return $this->db->insert_id();
    }

/**
 * This function used to update payment details after payment
 * @param {array} update data
 */

    function afterPaymentInsertModel( $data , $id )
    {   
        $this->db->where('payment_id', $id);
        $this->db->update('student_subs_payment', $data);
    }


}