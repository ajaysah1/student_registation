<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Awards And Certificates</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  </head>
  <body>
    <div class="container"  style="margin-top:5%;">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/index">Personal Info</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/HighSchool">High School</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/SkillsActivities">Skills & Activities</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/AwardCertificates">Awards & Certifications</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/WorkExperience">Work Experienc</a>
        </li>
      </ul>
      <!-- award section container -->
      <div class="tab-content">
          <h1><center>Awards & Certifications</center></h1>
          <div  class="container tab-pane active">
            <div class="container-md" style="margin-top:2%; margin-bottom:5%; border:2px solid;" >
              <form method="post">
        <!-- award  container start -->
              <div class="" id="project_field">
                  <button type="button" name="add" id="add_project" class="btn btn-success" style="float:right;">+</button>
                <fieldset style="border:1px solid blue; margin-top:2%; padding:2%;">
                <div class="form-row" >
                    <div class="col">
                      <label for="inputFname">Project Title</label>
                      <input type="text" class="form-control" name="title[]" placeholder="Project Title" required>
                    </div>
                </div>
    <!-- logic for  add project -->
                <script>
                $(document).ready(function(){
                	var i=1;
                	$('#add_project').click(function(){
                		i++;
                		$('#project_field').append('<div id="row'+i+'"><button type="button" style="float: right; margin-bottom:2%;" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button><fieldset style="border:1px solid; margin-top:2%; padding:2%; "><label>Projects</label><div class="form-row" style="margin-top:3%;"><div class="col"> <input type="text" class="form-control" name="title[]" placeholder="Project Title" required> </div>  </div>  <div class="form-row" style="margin-top:3%;"><div class="col"><input type="text" class="form-control" name="project_details[]" placeholder="Project Description" value=""></div></div><div class="form-row" style="margin-top:3%;"><div class="col"><input type="date" class="form-control" name="start_date[]" placeholder="End Date" value=""></div><div class="col"><div class="col"><input type="date" class="form-control" name="end_date[]" placeholder="Start Date" value=""></div></div></div></fieldset></div> ')
                	});

                	$(document).on('click', '.btn_remove', function(){
                		var button_id = $(this).attr("id");
                		$('#row'+button_id+'').remove();
                	});
                });
                </script>
      <!-- End logic for add project -->
                <div class="form-row" style="margin-top:3%;">
                  <div class="col">
                    <input type="text" class="form-control" name="project_details[]" placeholder="Project Description" value="">
                  </div>
                </div>
                <div class="form-row" style="margin-top:3%;">
                  <div class="col">
                    <input type="date" class="form-control" name="start_date[]" placeholder="End Date" value="">
                  </div>
                  <div class="col">
                    <div class="col">
                      <input type="date" class="form-control" name="end_date[]" placeholder="Start Date" value="">
                    </div>
                  </div>
                </div>
                    </div>
                  </fieldset>
        <!-- project Description end  -->
        <div class="" id="award_field">
            <button type="button" name="add" id="add_award" class="btn btn-success" style="float:right;">+</button>
          <fieldset style="border:1px solid blue; margin-top:2%; padding:2%;">
          <div class="form-row" >
              <div class="col">
                <label for="inputFname">Award</label>
                <input type="text" class="form-control" name="award[]" placeholder="award Name" required>
              </div>
          </div>
<!-- logic for  add award -->
          <script>
          $(document).ready(function(){
            var i=1;
            $('#add_award').click(function(){
              i++;
              $('#award_field').append('<div id="row'+i+'"><button type="button" style="float: right; margin-bottom:2%;" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button><fieldset style="border:1px solid; margin-top:2%; padding:2%; "><label>Awards</label><div class="form-row" style="margin-top:3%;"><div class="col"> <input type="text" class="form-control" name="award[]" placeholder="Award Title" required> </div></div></fieldset></div> ')
            });

            $(document).on('click', '.btn_remove', function(){
              var button_id = $(this).attr("id");
              $('#row'+button_id+'').remove();
            });
          });
          </script>
<!-- End logic for add award -->
              </div>
            </fieldset>


              <div class="" id="Certificate_field">
                  <button type="button" name="add" id="add_certifiacte" class="btn btn-success" style="float:right;">+</button>
                <fieldset style="border:1px solid blue; margin-top:2%; padding:2%;">
                <div class="form-row" >
                    <div class="col">
                      <label for="inputFname">Certifications</label>
                      <input type="text" class="form-control" name="title[]" placeholder="Cerificated Title" required>
                    </div>
                </div>
    <!-- logic for  add award -->
                <script>
                $(document).ready(function(){
                  var i=1;
                  $('#add_certifiacte').click(function(){
                    i++;
                    $('#Certificate_field').append('<div id="row'+i+'"><button type="button" style="float: right; margin-bottom:2%;" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button><fieldset style="border:1px solid; margin-top:2%; padding:2%; "><label>Certifications</label><div class="form-row" style="margin-top:3%;"><div class="col"> <input type="text" class="form-control" name="title[]" placeholder="Cerificated Title" required> </div>  </div><div class="form-row" style="margin-top:3%;"><div class="col"><input type="date" class="form-control" name="join_date[]" placeholder="End Date" value=""></div><div class="col"><div class="col"><input type="date" class="form-control" name="expire_date[]" placeholder="Start Date" value=""></div></div></div></fieldset></div> ')
                  });

                  $(document).on('click', '.btn_remove', function(){
                    var button_id = $(this).attr("id");
                    $('#row'+button_id+'').remove();
                  });
                });
                </script>
      <!-- End logic for add award -->

                <div class="form-row" style="margin-top:3%;">
                  <div class="col">
                    <input type="date" class="form-control" name="join_date[]" placeholder="End Date" value="">
                  </div>
                  <div class="col">
                    <div class="col">
                      <input type="date" class="form-control" name="expire_date[]" placeholder="Start Date" value="">
                    </div>
                  </div>
                </div>
                    </div>
                  </fieldset>

                <div class="form-row" style="margin-top:5%;">
                    <div class="col">
                      <a href="http://localhost/CI/public/index.php/Registration/SkillsActivities"><input type="button" name="" class="btn btn-primary" value="back"></a>
                    </div>

                  <div class="col">
                      <input type="submit" name="btn primary" value="Continue"  style="float: right; margin-bottom:2%;" class="btn btn-primary">
                  </div>
              </div>

              </form>
          </div>
        </div>

  </body>
</html>
