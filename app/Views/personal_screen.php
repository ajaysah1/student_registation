<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Personal Information</title>
    <link rel="stylesheet" href=".../assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </head>
  <body>



    <div class="container"  style="margin-top:5%;" >
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/index">Personal Info</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/HighSchool">High School</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/SkillsActivities">Skills & Activities</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/AwardCertificates">Awards & Certifications</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/WorkExperience">Work Experience</a>
        </li>

      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div class="container tab-pane active"><br>
          <h1><center>Personal Infromation</center></h1>
                <div class="container-md" style="margin-top:2%; margin-bottom: 5%;border:1px solid;" >
                  <?php if (session()->get('success')): ?>
                    <div class="alert alert-success">
                       <?= session()->get('success') ?>
                 <?= session()->get('student_id') ?>
                     </div>
                  <?php endif ?>
                  <?php if (isset($validation)): ?>
                <div class="col-12">
                  <div class="alert alert-danger" role="alert">
                    <?=  $validation->listErrors() ?>
                  </div>
                </div>
                <?php endif; ?>
                  <form method="post">
                      <div class="form-row">
                          <div class="col">
                            <label for="inputFname">First Name</label>
                            <?php
                            $user_data=session()->get('data_test');
                            foreach ($user_data as $user):?>
                            <input type="text" class="form-control" value="<?php echo $user['first_name']; ?>" name="firstname"placeholder="First Name" required>
                          </div>
                          <div class="col">
                            <label for="inputLname">Last Name</label>
                            <input type="text" class="form-control" name="lastname" value="<?php echo $user['last_name']; ?>"  placeholder="Last Name" required>
                          </div>
                      </div>
                      <div class="form-row" style="margin-top:3%;">
                          <div class="col">
                            <label for="inputEmail">Email</label>
                            <input type="email" class="form-control" value="<?php echo $user['email_id']; ?>" name="email" placeholder="Email" required>
                          </div>
                          <div class="col">
                            <label for="password">Password</label>
                            <input type="password" class="form-control"   name="password" value="<?php echo $user['password']; ?>" placeholder="Password" required>
                          </div>
                          <div class="col">
                            <label for="password">Conform Password</label>
                            <input type="password" class="form-control" name="conform_password" value="<?php echo $user['password']; ?>" placeholder="Conform Password" required>
                          </div>

                      </div>


                    <div class="form-row" style="margin-top:3%;">
                      <div class="col">
                         <label for="dropdownState">State</label>
                         <select class="form-control" name="state" onchange="getCity();" id="selectState">
                           <?php foreach ($std_data as $row):?>
                           <option value="<?= $row['state_id']; ?>"   <?php if ($row['state_id']==$user['state_id']){
                             echo "selected";
                           } ?>
                               ><?= $row['state_names']; ?></option>
                         <?php endforeach; ?>
                         </select>
                      </div>
                      <div class="col">
                         <label for="dropdownCity">City</label>
                         <select class="form-control" name="city" id="selectCity">
                          <option></option>
                         </select>
                      </div>
                    </div>

                    <script type="text/javascript">


                      // onchange state to get city
                      function getCity() {
                        var selectState = $("#selectState").val();

                            $.ajax({
                                type:"post",
                                url: "<?php echo base_url().'/Registration/getCity' ?>",
                                data:{ selectState : selectState },
                                success:function(response){
                                    //alert(response);
                                    $('#selectCity').html(response);
                                    $('#selectCity').find('option').get(0).remove();
                                    beforeSchoolCity();
                                }
                            });


                      }

                    </script>

                    <div class="form-row" style="margin-top:3%;" >
                      <div class="col">
                         <label for="inputphone">Phone Number</label>
                        <input type="tel" class="form-control" value="<?php echo $user['phone_num']; ?>"  name="phone_num" placeholder="(123)456789">
                      </div>
                      <div class="col">
                         <label for="inputEmail4">Parent's Email ID</label>
                        <input type="text" class="form-control"  name="parent_email" value="<?php echo $user['parent_email']; ?>"  placeholder="abc@gmail.com">
                      </div>
                    </div>

                    <div class="form-row" style="margin-top:3%;" >
                      <div class="col">
                         <label for="inputphone">Instagram Handle</label>
                        <input type="tel" class="form-control" placeholder="@" name="insta_handle" value="<?php echo $user['insta_handler']; ?>" >
                      </div>
                      <div class="col">
                         <label for="inputlink">Website/Blog/Linkedin</label>
                        <input type="text" class="form-control" placeholder="link" name="website" value="<?php echo $user['website_link']; ?>">
                      </div>
                    </div>

                <div class="form-row" style="margin-top:3%;">
                <div class="col">

                </div>
                <div class="col">
                  <a href="http://localhost/CI/public/index.php/Registration/HighSchool"> <input type="submit" name="submit" value="Continue"  style="float: right; margin-bottom:2%;" class="btn btn-primary"></a>
                </div>
                </div>
              <?php endforeach; ?>
              </form>
            </div>
        </div>
      </div>
    </div>































  </body>
</html>
