<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>High School</title>
    <link rel="stylesheet" href=".../assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </head>
  <body>

        <div class="container"  style="margin-top:5%;">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/index">Personal Info</a>
            </li>
            <li class="nav-item">
              <a class="nav-link  active" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/HighSchool">High School</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/SkillsActivities">Skills & Activities</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/AwardCertificates">Awards & Certifications</a>
            </li>

            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/WorkExperience">Work Experienc</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="container tab-pane fade active"><br>
            </div>
            <div class="container tab-pane active"><br>
              <h1><center>High School</center></h1>
              <div class="container-md" style="margin-top:2%; margin-bottom:5%; border: 1px solid;" >
                <?php if (session()->get('success')): ?>
                  <div class="alert alert-success">
                     <?= session()->get('success') ?>
                  </div>
                  <div>


                  </div>
                <?php endif ?>

                <?php if (isset($validation)): ?>
              <div class="col-12">
                <div class="alert alert-danger" role="alert">
                  <?=  $validation->listErrors() ?>

                </div>
              </div>
              <?php endif; ?>

                <form method="post">
                    <div class="form-row">
                        <div class="col">
                          <label for="SchoolCity">School City</label>
                          <select class="form-control" name="school_city">
                            <option value="1">City1</option>
                            <option value="2">City2</option>
                          </select>
                        </div>
                        <div class="col">
                          <label for="schoolName">School Name</label>
                          <input type="text" name="school_name" class="form-control" value="">
                        </div>
                    </div>
                  <div class="" id="dynamic_subject">
                    <!-- <button type="button" name="add_subj" id="add" class="btn btn-success" style="float:right;">+</button> -->
                    <p style="margin-top:3%;" >High School Subjects</p>
                    <div class="form-row" >
                        <div class="col">
                          <select class="form-control" name="subject[]">
                            <?php foreach ($subject_data as $row):?>
                              <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <!-- <script>
                        $(document).ready(function(){
                          var i=1;
                          $('#add').click(function(){
                            i++;
                            $('#dynamic_subject').append('<div id="row'+i+'"><button type="button" style="float: right; margin-bottom:2%;" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button><div class="form-row" style="margin-top:2%;" ><div class="col"><select class="form-control" name="subject[]"><?php foreach($subject_data as $row):?><option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option><?php endforeach ?></select></div><div class="col"><select class="form-control" name="subject[]"><option value="1">Computer Science</option><option value="2">Nepali</option><option value="3">English</option></select></div></div></div>')
                          });

                          $(document).on('click', '.btn_remove', function(){
                            var button_id = $(this).attr("id");
                            $('#row'+button_id+'').remove();
                          });
                        });
                        </script> -->
                        <div class="col">
                          <select class="form-control" name="subject[]">
                            <?php foreach ($subject_data as $row):?>
                              <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                            <?php endforeach ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-row" style="margin-top:2%;">
                        <div class="col">
                          <select class="form-control" name="subject[]">
                            <?php foreach ($subject_data as $row):?>
                              <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                            <?php endforeach ?>
                            </select>
                              </div>
                        <div class="col">
                          <select class="form-control" name="subject[]">
                            <?php foreach ($subject_data as $row):?>
                              <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                    </div>    <div class="form-row" style="margin-top:2%;">
                            <div class="col">
                              <select class="form-control" name="subject[]">
                                <?php foreach ($subject_data as $row):?>
                                  <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                                <?php endforeach ?>
                                </select>
                              </div>
                            <div class="col">
                              <select class="form-control" name="subject[]">
                                <?php foreach ($subject_data as $row):?>
                                  <option value="<?php echo $row['subject_id']; ?>"><?php echo $row['subject_name']; ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                        </div>
                    </div>
                  <div class="form-row" style="margin-top:5%;">
                    <div class="col">
                       <label for="dropdownState">GPA (Unweighted)</label>
                       <select class="form-control" name="gpa_unweg">
                         <option value="1">1</option>
                          <option value="2">2</option>
                       </select>
                    </div>
                    <div class="col">
                       <label for="dropdownCity">GPA (Weighted)</label>
                       <select class="form-control" name="gpa_wei">
                         <option value="1">1</option>
                         <option value="2">2</option>
                       </select>
                    </div>
                  </div>


            <div class="form-row" style="margin-top:5%;">
              <div class="col">
            <a href="http://localhost/CI/public/index.php/Registration/index" ><input type="button" name="" class="btn btn-primary" value="back"></a> 
              </div>

              <div class="col">
            <input type="submit" name="btn primary" value="Continue"  style="float: right; margin-bottom:2%;" class="btn btn-primary">
              </div>
            </div>

                </form>
              </div>
          </div>
          </div>







  </body>
</html>
