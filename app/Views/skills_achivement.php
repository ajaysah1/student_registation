<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Skills And Achivement</title>
    <link rel="stylesheet" href=".../assets/css/bootstrap-grid.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="container"  style="margin-top:5%;">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/index">Personal Info</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/HighSchool">High School</a>
        </li>
        <li class="nav-item">
          <a class="nav-link active" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/SkillsActivities">Skills & Activities</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/AwardCertificates">Awards & Certifications</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" data-toggle="tab" href="http://localhost/CI/public/index.php/Registration/WorkExperience">Work Experienc</a>
        </li>

      </ul>
      <div class="tab-content">
        <div  class="container tab-pane active">
          <h1><center>Skills & Activities</center></h1>
          <div class="container-md" style="margin-top:2%; margin-bottom: 5%;border:1px solid;" >
            <form method="post">
              <div class="form-row">
                  <div class="col" style="margin-top:2%;">
                      <label for="SchoolCity">Skills</label>
                  <textarea name="name" class="form-control"  rows="8" cols="80"></textarea>
                  </div>
                    </div>
                  <div class="form-row">
                      <div class="col" style="margin-top:2%;">
                        <label for="Extracurricular">Extracurricular Activities</label>
                        <select name="extra_activities[]" onclick="getExtraActivities()" class="extracurricular-activities-select form-control" required="required" multiple="multiple" id="extra_activities">
                        </select>
                      </div>
                    </div>
<script type="text/javascript">
function getExtraActivities() {

      $.ajax({
          type:"post",
          url: "<?php echo base_url().'/Registration/getExtraActivities' ?>",
          success:function(response){
              $('#extra_activities').html(response);
          }
      });
}
</script>
                      <div class="form-row" style="margin-top:5%;">
                        <div class="col">
                        <a href="http://localhost/CI/public/index.php/Registration/HighSchool"> <input type="button" name="" class="btn btn-primary" value="back"></a> 
                        </div>

                        <div class="col">
                      <input type="submit" name="btn primary" value="Continue"  style="float: right; margin-bottom:2%;" class="btn btn-primary">
                        </div>
                      </div>
            </form>

          </div>
        </div>
  </div>
  </div>

  </body>
</html>
