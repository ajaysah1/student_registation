<!DOCTYPE html>
<html>
<?php $this->load->view('Website/head'); ?>
<style type="text/css">
	footer {
		display: none;
	}
</style>
<body>
<?php $this->load->view('Website/nav'); ?>

	<!-- Admin Signup -->
<div class="container">
	<div class="row">
	<section class="student-step-registration">
		<div class="row side-form">
			<div class="col-md-1">
				
			</div>
			<div class="col-md-11">

				<div class="header-top-form"></div>

				<div class="header-step-form">
					<div class="row">
						<div class="col-md-12">
							<div class="form-steps" id="personalInformationStep">
								<span class="step-circle active">1</span><br>
								<span class="step-name active">Personal Information</span>
							</div>
							<div class="form-steps" id="highSchoolStep">
								<span class="step-circle">2</span><br>
								<span class="step-name">Education Details</span>
							</div>
							<div class="form-steps" id="skillsActivitiesStep">
								<span class="step-circle ">3</span><br>
								<span class="step-name ">Skills & Activities</span>
							</div>
							<div class="form-steps" id="awardsCertificationsStep">
								<span class="step-circle ">4</span><br>
								<span class="step-name ">Awards & Certifications</span>
							</div>
							<div class="form-steps" id="workExperienceStep">
								<span class="step-circle ">5</span><br>
								<span class="step-name ">Work Experience</span>
							</div>
						</div>
					</div>
				</div>

				<div id="header-step-process" class="header-step-process-form">
					
				</div>

				<div class="header-content-form-wrap">

					<div class="personal-information" id="personalInfomationContent">
						<form id="save_step_1" action="<?php echo base_url().'student/savereg?save_step=1'; ?>" method="post">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                               <label>Name<spam style="color: red;">*</spam> </label>
                               <input placeholder="Enter Name" type="text" name="s_name" id="s_name" class="form-control" value="<?php echo $users[0]['name']; ?>" disabled required="required">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Email ID<spam style="color: red;">*</spam> </label>
                               <input placeholder="xyz@gmail.com" type="email" name="s_email" id="s_email" value="<?php echo $users[0]['email']; ?>" disabled class="form-control" required="required">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>State<spam style="color: red;">*</spam></label>
                                <br>
                                <select onchange="getCity();" id="selectState" name="state" class="form-control" required="required">
                                	<option></option>
                                	<?php foreach ($states as $key) {
                                		
                                	?>
                                		<option <?php if(isset($s_state)){
                                			if( $key['id'] == $s_state ){
                                				echo "selected";
                                			}
                                		} ?> value="<?php echo $key['id'] ?>"> <?php echo $key['name'] ?> </option>
                                	<?php } ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>City<spam style="color: red;">*</spam></label>
                                <br>
                                <select id="selectCity" name="city" class="form-control" required="required">
                                	<option></option>
                                	<?php
                                		if (isset($city)) {
                                			foreach ($city as $row) {
                                	?>
                                		<option <?php if( $s_city == $row['id'] ) {
                                			echo "selected";
                                		} ?> value="<?php echo $row['id'] ?>" > <?php echo $row['name'] ?> </option>
                                	<?php
                                			}
                                		}
                                	?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Phone Number<spam style="color: red;">*</spam> </label>
                               <input placeholder="(123) 456 7893" type="number" name="s_mobile" id="phonenumber" class="form-control" required="required" <?php if(isset($s_mobile)){
                               	echo 'value="'.$s_mobile.'"';
                               } ?> ><span id="message" style="color: red; position: absolute;"></span>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Parent’s Email ID<spam style="color: red;">*</spam> </label>
                               <input placeholder="xyz@gmail.com" type="email" name="p_email" id="p_email" class="form-control" required <?php if(isset($par_mail)){
                               	echo 'value="'.$par_mail.'"';
                               } ?> >
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Instagram</label>
                               <input placeholder="@" type="text" name="s_instagram" id="s_instagram" class="form-control" <?php if(isset($insta)){
                               	echo 'value="'.$insta.'"';
                               } ?> >
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Website </label>
                               <input placeholder="Link" type="text" name="s_website" id="s_website" class="form-control" <?php if(isset($website)){
                               	echo 'value="'.$website.'"';
                               } ?> >
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Blog </label>
                               <input placeholder="Link" type="text" name="s_blog" id="s_blog" class="form-control" <?php if(isset($blog)){
                               	echo 'value="'.$blog.'"';
                               } ?> >
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>LinkedIn </label>
                               <input placeholder="Link" type="text" name="s_linkedin" id="s_linkedin" class="form-control" <?php if(isset($linkedin)){
                               	echo 'value="'.$linkedin.'"';
                               } ?> >
                            </div>
                        </div>

						<div class="step-navigation col-md-12">
							<button id="btn_reg_step_1" class="next-btn" onclick="regiNext('personalInformationStep', 'personalInfomationContent', 'highSchoolStep','highSchoolContent', 'step-two', 'save_step_1')" >Continue</button>
						</div>

						</form>
					</div>

					<div class="high-school step-hidden" id="highSchoolContent">
						<form id="save_step_2" action="<?php echo base_url().'student/savereg?save_step=2'; ?>" method="post">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>City<spam style="color: red;">*</spam></label>
                                <select id="selectSchoolCity" name="school_city" class="form-control" required="required">
                                	<option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>School Name<spam style="color: red;">*</spam></label>
                                <select id="selectSchool" name="s_school" class="form-control" required >
									<option></option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>High School Subjects<spam style="color: red;">*</spam></label><br>
                                <select id="high_school_subject" name="high_school_subject[]" class="high-school-subjects form-control" required="required" multiple="multiple">
                                	<option></option>
                                	<?php
                                		foreach ($subject as $row) {
                                	?>
                                		<option <?php if( in_array($row['id'], $student_subject) )
                                						{ echo "selected"; }  ?> value="<?php echo $row['id']; ?>"><?php echo $row['name']; ?></option>
                                	<?php
                                		}
                                	?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>GPA (Unweighted) <spam style="color: red;">*</spam></label>
                               
                               <select name="gpa_un" id="gpa_un" class="form-control" required>
                               		<?php
                               			for($i=0.0; $i <= 4.0;) { 
                               		?>
                               		<option <?php if(isset($gpa)) {if ($gpa == $i) {echo "selected";}} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                               		<?php
                               			$i = round($i + 0.1, 1);
                               			}
                               		?>
                               </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>GPA (Weighted) </label>
                               <select name="gpa_wei" id="gpa_wei" class="form-control" required>
                               		<?php
                               			for($i=0.0; $i <= 5.0;) { 
                               		?>
                               		<option <?php if(isset($gpa_wei)) {if ($gpa_wei == $i) {echo "selected";}} ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                               		<?php
                               			$i = round($i + 0.1, 1);
                               			}
                               		?>
                               </select>
                            </div>
                        </div>

						<div class="step-navigation col-md-12">
							<button class="back-btn" onclick="regiNext('highSchoolStep','highSchoolContent','personalInformationStep', 'personalInfomationContent', 'step-one'), event.preventDefault();" >Back</button>
							<button id="btn_reg_step_2" class="next-btn" onclick="regiNext('highSchoolStep', 'highSchoolContent', 'skillsActivitiesStep','skillActivitiesContent', 'step-three', 'save_step_2')" >Continue</button>
						</div>
						</form>
					</div>


					<div class="skill-activities step-hidden" id="skillActivitiesContent">
						<form id="save_step_3" action="<?php echo base_url().'student/savereg?save_step=3'; ?>" method="post">

                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group">
                            <div class="form-group col-md-12 col-sm-12">
                                <label>Skills <spam style="color: red;">*</spam></label><br>
                                <select name="skill[]" id="skill" class="skils-select form-control" required="required" multiple="multiple">
                                	
                                </select>
                            </div>
                        

                        
                            <div class="form-group col-md-12 col-sm-12">
                                <label>Extracurricular Activities <spam style="color: red;">*</spam></label><br>
                                <select name="extra_activities[]" class="extracurricular-activities-select form-control" required="required" multiple="multiple" id="extra_activities">
                                	
                                </select>
                            </div>
                        	</div>
                        </div>

						<div class="step-navigation col-md-12">
							<button class="back-btn" onclick="regiNext('skillsActivitiesStep','skillActivitiesContent','highSchoolStep', 'highSchoolContent', 'step-two'), event.preventDefault();" >Back</button>
							<button id="btn_reg_step_3" class="next-btn" onclick="regiNext('skillsActivitiesStep','skillActivitiesContent', 'awardsCertificationsStep','awardsCertificationsContent', 'step-four', 'save_step_3')" >Continue</button>
						</div>
						</form>
					</div>

					<div class="awards-certifications step-hidden" id="awardsCertificationsContent">
						<form id="save_step_4" action="<?php echo base_url().'student/savereg?save_step=4'; ?>" method="post">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Projects</label><br>

                    <?php
                    	if( empty($student_project) ) {
                    ?>
                                <div class="form-group col-md-6 col-sm-6">
                                	<input type="text" class="form-control" name="project_title[]" placeholder="Project Title">
                                </div>

                                <div class="form-group col-md-3 col-sm-3 ">
                                	<input type="text" class="form-control select_date" name="project_start[]" placeholder="Start Date">
                                </div>

                                <div class="form-group col-md-3 col-sm-3 ">
                                	<input type="text" class="form-control select_date" name="project_end[]" placeholder="End Date">
                                </div>
	                            <span class="swap-rows-mobile">

	                                <div class="form-group col-md-12 col-sm-12">
	                                	<textarea type="text" class="form-control" name="project_des[]" rows="1" placeholder="Project Description: Tell us briefly about any type of projects you have successfully completed or are working on."></textarea>
	                                </div>
	                                
	                            </span>
	                            
                    <?php
                    	}else{
                    		$student_project_check = NULL;
                    		foreach ($student_project as $key) {
                    			if ( $student_project_check == NULL ) {
                    				$student_project_check = "some";
                    ?>
                                <div class="form-group col-md-6 col-sm-6">
                                	<input type="text" class="form-control" name="project_title[]" placeholder="Project Title" value="<?php echo $key['project_title'] ?>">
                                </div>

                                <div class="form-group col-md-3 col-sm-3 ">
                                	<input type="text" class="form-control select_date" name="project_start[]" placeholder="Start Date" value="<?php echo $key['start_date'] ?>">
                                </div>

                                <div class="form-group col-md-3 col-sm-3 ">
                                	<input type="text" class="form-control select_date" name="project_end[]" placeholder="End Date" value="<?php echo $key['end_date'] ?>">
                                </div>
	                            <span class="swap-rows-mobile">
	                                <div class="form-group col-md-12 col-sm-12">
	                                	<textarea type="text" class="form-control" name="project_des[]" rows="1" placeholder="Project Description: Tell us briefly about any type of projects you have successfully completed or are working on"><?php echo $key['pro_desc'] ?></textarea>
	                                </div>
	                            </span>
								
                    <?php
                    			}else{
                    ?>
									<span class="more-field-top-border">
										<div class="form-group col-md-6 col-sm-6"> 
											<input type="text" class="form-control" name="project_title[]" placeholder="Project Title" value="<?php echo $key['project_title'] ?>" > 
										</div> 
										<div class="form-group col-md-2 col-sm-2 "> 
											<input type="text" class="form-control select_date" name="project_start[]" placeholder="Start Date" value="<?php echo $key['start_date'] ?>" > 
										</div>
										 <div class="form-group col-md-2 col-sm-2 "> 
										 	<input type="text" class="form-control select_date" name="project_end[]" placeholder="End Date" value="<?php echo $key['end_date'] ?>" > 
										 </div>
										 <span class="swap-rows-mobile">
										 	<div class="form-group col-md-2 col-sm-2"> <i onclick="projectRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div> 
										 	<div class="form-group col-md-12 col-sm-12">
										 	 <textarea type="text" class="form-control" name="project_des[]" rows="1" placeholder="Project Description: Tell us briefly about any type of projects you have successfully completed or are working on"> <?php echo $key['pro_desc'] ?> </textarea> 
										 	</div>
										 </span>
									</span>
                    <?php
                    			}
                    		}
                    	}
                    ?>
                    			<span id="add_more_project"></span>
								<div class="form-group col-md-4 col-sm-4">
									<i id="add_project" class="add-more-field fa fa-plus"> <span class="label-desc"> Add Another Project </span></i>
	                            </div>

                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Awards</label><br>
                    <?php
                    	if( empty($student_award) ) {
                    ?>
                                <div class="form-group col-md-6 col-sm-6">
                                	<input type="text" class="form-control" name="award_name[]" placeholder="Award Name">
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="award_date[]" placeholder="Award Date">
                                </div>

								<span class="swap-rows-mobile">

	                                <div class="form-group col-md-12 col-sm-12">
	                                	<textarea type="text" class="form-control" name="award_des[]" rows="1" placeholder="Award Description: Tell us briefly about any type of awards you have received and why it was presented to you"></textarea>
	                                </div>
								</span>

                    <?php
                    	}else{
                    		$student_award_check = NULL;
                    		foreach ($student_award as $key) {
                    			if ( $student_award_check == NULL ) {
                    				$student_award_check = "some";
                    ?>
                                <div class="form-group col-md-6 col-sm-6">
                                	<input type="text" class="form-control" name="award_name[]" placeholder="Award Name" value="<?php echo $key['award_title'] ?>">
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="award_date[]" placeholder="Award Date" value="<?php echo $key['award_date'] ?>">
                                </div>

								<span class="swap-rows-mobile">

	                                <div class="form-group col-md-12 col-sm-12">
	                                	<textarea type="text" class="form-control" name="award_des[]" rows="1" placeholder="Award Description: Tell us briefly about any type of awards you have received and why it was presented to you"><?php echo $key['award_desc']; ?></textarea>
	                                </div>

								</span>

                    <?php
                    			}else{
                    ?>
									<span class="more-field-top-border">
										<div class="form-group col-md-6 col-sm-6">
											<input type="text" class="form-control" name="award_name[]" placeholder="Award Name" value="<?php echo $key['award_title'] ?>" >
										</div>
										<div class="form-group col-md-4 col-sm-4 ">
											<input type="text" class="form-control select_full_date" name="award_date[]" placeholder="Award Date" value="<?php echo $key['award_date'] ?>" >
										</div>
										<span class="swap-rows-mobile">
											<div class="form-group col-md-2 col-sm-2"> <i onclick="projectRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> 
											</div>
											<div class="form-group col-md-12 col-sm-12">
												<textarea type="text" class="form-control" name="award_des[]" rows="1" placeholder="Award Description: Tell us briefly about any type of awards you have received and why it was presented to you"> <?php echo $key['award_desc']; ?> </textarea>
											</div>
										</span>
									</span>
                    <?php
                    			}
                    		}
                    	}
                    ?>
                                <span id="add_more_award"></span>
								<div class="form-group col-md-4 col-sm-4">
	                            	<i id="add_award" class="add-more-field fa fa-plus"> <span class="label-desc"> Add Another Award </span></i>
	                            </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Certifications</label><br>

                    <?php
                    	if( empty($student_cert) ) {
                    ?>
                                <div class="form-group col-md-4 col-sm-4">
                                	<input type="text" class="form-control" name="cert_name[]" placeholder="Certificate Name">
                                </div>

                                <div class="form-group col-md-4 col-sm-4">
                                	<input type="text" class="form-control" name="issue_auth[]" placeholder="Issuing Authority">
                                </div>

                                <div class="form-group col-md-4 col-sm-4 ">
                                	<input type="text" class="form-control select_full_date" name="cert_date[]" placeholder="Certificate Date">
                                </div>

                    <?php
                    	}else{
                    		$student_cert_check = NULL;
                    		foreach ($student_cert as $key) {
                    			if ( $student_cert_check == NULL ) {
                    				$student_cert_check = "some";
                    ?>
                                <div class="form-group col-md-4 col-sm-4">
                                	<input type="text" class="form-control" name="cert_name[]" placeholder="Certificate Name" value="<?php echo $key['cert_name'] ?>" >
                                </div>

                                <div class="form-group col-md-4 col-sm-4">
                                	<input type="text" class="form-control" name="issue_auth[]" placeholder="Issuing Authority" value="<?php echo $key['issue_auth'] ?>" >
                                </div>

                                <div class="form-group col-md-4 col-sm-4 ">
                                	<input type="text" class="form-control select_full_date" name="cert_date[]" placeholder="Certificate Date" value="<?php echo $key['cert_date'] ?>" >
                                </div>

                    <?php
                    			}else{
                    ?>
									<span class="more-field-top-border">
										<div class="form-group col-md-4 col-sm-4">
											<input type="text" class="form-control" name="cert_name[]" placeholder="Certificate Name" value="<?php echo $key['cert_name'] ?>" >
										</div>
										<div class="form-group col-md-4 col-sm-4">
											<input type="text" class="form-control" name="issue_auth[]" placeholder="Issuing Authority" value="<?php echo $key['issue_auth'] ?>" >
										</div>
										<div class="form-group col-md-4 col-sm-4 ">
											<input type="text" class="form-control select_full_date" name="cert_date[]" placeholder="Certificate Date" value="<?php echo $key['cert_date'] ?>" >
										</div>
										<div class="form-group col-md-2 col-sm-2"> <i onclick="certRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div>
									</span>
                    <?php
                    			}
                    		}
                    	}
                    ?>
                                <span id="add_more_cert"></span>
								<div class="form-group col-md-4 col-sm-4">
                                	<i id="add_cert" class="add-more-field fa fa-plus"> <span class="label-desc"> Add Another Certification </span></i>
                                </div>

                            </div>
                        </div>

						<div class="step-navigation col-md-12">
							<button class="back-btn" onclick="regiNext('awardsCertificationsStep', 'awardsCertificationsContent','skillsActivitiesStep', 'skillActivitiesContent', 'step-three'), event.preventDefault();" >Back</button>
							<button class="next-btn" onclick="regiNext('awardsCertificationsStep', 'awardsCertificationsContent', 'workExperienceStep','workExperienceContent', 'step-five', 'save_step_4')" >Continue</button>
							<button id="btn_reg_step_4" class="skip-btn" onclick="regiNext('awardsCertificationsStep', 'awardsCertificationsContent', 'workExperienceStep','workExperienceContent', 'step-five'), event.preventDefault();" >Skip</button>
						</div>

						</form>

					</div>

					<div class="work-experience step-hidden" id="workExperienceContent">

						<form id="save_step_5" action="<?php echo base_url().'student/savereg?save_step=5'; ?>" method="post" enctype="multipart/form-data" >
							<input id="redirValue" hidden type="text" name="redir_value">
                        <div class="col-md-12 col-sm-12">

<?php
if ( $this->session->flashdata('error_msg') ) {
?>
<div class="alert alert-warning" role="alert">
 <?php print_r( $this->session->flashdata('error_msg') ); ?>
</div>
<?php } ?>

                            <div class="form-group">
                                <label>Work Experience</label><br>
                                <span class="label-desc">Tell us in detail about any full or part-time employment, volunteering, and/or community service experiences</span>

                    <?php
                    	if( empty($student_work_exp) ) {
                    ?>
								<div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_title[]" placeholder="Title">
                                </div>

								<div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_company[]" placeholder="Company Name">
                                </div>

                                <div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_location[]" placeholder="Location">
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="work_start[]" placeholder="Start Date">
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="work_end[]" placeholder="End Date">
                                </div>

                    <?php
                    	}else{
                    		$student_work_check = NULL;
                    		foreach ($student_work_exp as $key) {
                    			if ( $student_work_check == NULL ) {
                    				$student_work_check = "some";
                    ?>
								<div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_title[]" placeholder="Title" value="<?php echo $key['work_title']; ?>" >
                                </div>

								<div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_company[]" placeholder="Company Name" value="<?php echo $key['company_name']; ?>" >
                                </div>

                                <div class="form-group col-md-12 col-sm-12">
                                	<input type="text" class="form-control" name="work_location[]" placeholder="Location" value="<?php echo $key['location']; ?>" >
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="work_start[]" placeholder="Start Date" value="<?php echo $key['start_date']; ?>" >
                                </div>

                                <div class="form-group col-md-6 col-sm-6 ">
                                	<input type="text" class="form-control select_full_date" name="work_end[]" placeholder="End Date" value="<?php echo $key['end_date']; ?>" >
                                </div>

                    <?php
                    			}else{
                    ?>
									<span class="more-field-top-border">
										<div class="form-group col-md-12 col-sm-12">
											<input type="text" class="form-control" name="work_title[]" placeholder="Title" value="<?php echo $key['work_title']; ?>" >
										</div>
										<div class="form-group col-md-12 col-sm-12">
											<input type="text" class="form-control" name="work_company[]" placeholder="Company Name" value="<?php echo $key['company_name']; ?>" >
										</div>
										<div class="form-group col-md-12 col-sm-12">
											<input type="text" class="form-control" name="work_location[]" placeholder="Location" value="<?php echo $key['location']; ?>" >
										</div>
										<div class="form-group col-md-6 col-sm-6 ">
											<input type="text" class="form-control select_full_date" name="work_start[]" placeholder="Start Date" value="<?php echo $key['start_date']; ?>" >
										</div>
										<div class="form-group col-md-6 col-sm-6 ">
											<input type="text" class="form-control select_full_date" name="work_end[]" placeholder="End Date" value="<?php echo $key['end_date']; ?>" >
										</div>
										<div class="form-group col-md-4 col-sm-4"> <i onclick="certRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i>
										</div>
									</span>
                    <?php
                    			}
                    		}
                    	}
                    ?>

                                <span id="add_more_work"></span>

								<div class="form-group col-md-4 col-sm-4">
                                	<i id="add_work" class="add-more-field fa fa-plus"> <span class="label-desc"> Add Another Experience </span> </i>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label>Career Goals</label><br>
                                <div class="form-group">
                                	<textarea type="text" class="form-control" name="career_goals" rows="1" placeholder="You set career goals by thinking about what you want to do, career-wise, in both the short-term and long-term sense. Think about: Your career interests. The industries that your talents and skills align with."><?php if ( isset($career_goals) ) {
                                		echo $career_goals;
                                	} ?></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 col-sm-12">
                        	<div class="form-group">
	                            <label>Resume (Optional)</label><br>
	                            <span class="label-desc">PDF and Word Formats are acceptable</span>
	                            <div class="form-group ">
									<div class="upload-btn-wrapperr">
									  <button class="upload-btn btn"> <i class="fa fa-plus"></i> Upload</button>
									  <input type="file" name="file_resume" id="file_resume">
									  <div id="file-upload-filename"></div>
									  <?php if ( isset($resume) ) { ?>
									  	<a target="_blank" href="<?php echo base_url().'uploads/resume/'.$resume; ?>">Click to view</a>
									  <?php } ?>
									  <div></div>
									
									</div>
	                            </div>
                        	</div>
                        </div>



						<div class="step-navigation col-md-12">
							<button class="back-btn" onclick="regiNext('workExperienceStep',
'workExperienceContent','awardsCertificationsStep', 'awardsCertificationsContent', 'step-four'),event.preventDefault();" >Back</button>
							<button id="finalSubmitModal" type="submit" class="next-btn" >Submit</button>
							
						</div>
						</form>
					</div>


				</div>
				
			</div>
		</div>
	</section>
	</div>
</div>
	<!-- End Admin Signup -->

<!-- After Sign Up Modal -->
<div class="modal fade" id="afterSignUp" tabindex="-1" role="dialog" aria-labelledby="afterSignUpLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<p>Help us get to know you.</p>
		<p>Fill in your details below to access quizzes, courses and apply to internships.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="blue-btn" data-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>

<!-- After Final Submit Modal -->
<div class="modal fade" id="afterSubmit" tabindex="-1" role="dialog" aria-labelledby="finalSubmitLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		<p>Welcome <b><?php echo $users[0]['name']; ?></b> </p>
		<br>
		<p>Congratulations! Let’s get started!</p>
		<p>Click on career map test to know more about yourself</p>
      </div>
      <div class="modal-footer">
        <button style="border: blue;" type="button" id="afterRegCan" class="blue-btn" data-dismiss="modal">Cancel</button>
        <button style="width: 228px; background: #c5001a; border: red;" type="button" id="afterRegCareer" class="blue-btn" data-dismiss="modal">Career Map Test</button>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('Website/footer'); ?>
<script type="text/javascript">
$(".select_date").attr("readonly","true");
$("input.select_date").css("background-color","#fff");

$(".select_full_date").attr("readonly","true");
$("input.select_full_date").css("background-color","#fff");

	<?php
		if ($reg_step == 1) {
	?>
	$('#afterSignUp').modal('show');
	<?php } ?>

	<?php
		if ($complete == 0) {
	?>
	
	$("#finalSubmitModal").click(function() {
		event.preventDefault();
		$('#afterSubmit').modal('show');

	});

	$("#afterRegCan").click(function() {
		$("#redirValue").val("cancel");
		$("#save_step_5").submit();
	});

	$("#afterRegCareer").click(function() {
		$("#redirValue").val("career");
		$("#save_step_5").submit();
	});

	<?php } ?>

var input = document.getElementById( 'file_resume' );
var infoArea = document.getElementById( 'file-upload-filename' );

input.addEventListener( 'change', showFileName );

function showFileName( event ) {
  var input = event.srcElement;
  var fileName = input.files[0].name;
  infoArea.textContent = 'File name: ' + fileName;
}

 var form_success;
$.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	return results[1] || 0;
}
  // Begin Get step
  	var reg_step = null;
  	reg_step = $.urlParam('reg_step');

  	if ( reg_step == '1' ) {

  	}

  	if ( reg_step == '2' ) {
  		// $("#btn_reg_step_1").click();
  		  	regiNext('personalInformationStep', 'personalInfomationContent', 'highSchoolStep','highSchoolContent', 'step-two');
  	}

  	if ( reg_step == '3' ) {
  		// $("#btn_reg_step_2").click();
  		// alert("new");
  		regiNext('personalInformationStep', 'personalInfomationContent', 'highSchoolStep','highSchoolContent', 'step-two');
		regiNext('highSchoolStep', 'highSchoolContent', 'skillsActivitiesStep','skillActivitiesContent', 'step-three');
  	}

  	if ( reg_step == '4' ) {
  		// $("#btn_reg_step_3").click();
  		regiNext('personalInformationStep', 'personalInfomationContent', 'highSchoolStep','highSchoolContent', 'step-two');
		regiNext('highSchoolStep', 'highSchoolContent', 'skillsActivitiesStep','skillActivitiesContent', 'step-three');
		regiNext('skillsActivitiesStep', 'skillActivitiesContent', 'awardsCertificationsStep','awardsCertificationsContent', 'step-four');
  	}

  	if ( reg_step == '5' ) {
  		// $("#btn_reg_step_4").click();
  		regiNext('personalInformationStep', 'personalInfomationContent', 'highSchoolStep','highSchoolContent', 'step-two');
		regiNext('highSchoolStep', 'highSchoolContent', 'skillsActivitiesStep','skillActivitiesContent', 'step-three');
		regiNext('skillsActivitiesStep', 'skillActivitiesContent', 'awardsCertificationsStep','awardsCertificationsContent', 'step-four');
		regiNext('awardsCertificationsStep', 'awardsCertificationsContent', 'workExperienceStep','workExperienceContent', 'step-five');

  	}
  // End Get step

  	// Date picker
  	selectDate();
    function selectDate() {
    	// $("#extraCssUI").remove();
    	$("body").append('<style id="extraCssUI" type="text/css">.ui-datepicker-calendar { display: none !important; }</style> ');

	    $('.select_date').focus(function(){
	    	$(".ui-datepicker-calendar").hide();
	    }).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'MM / yy',
	        create: function (input, inst) { 
	            
	         },
	        onClose: function(dateText, inst) { 
	        	var month = parseInt($("#ui-datepicker-div .ui-datepicker-month :selected").val());           
	            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	            $(this).val($.datepicker.formatDate('MM / yy', new Date(year, month)));
	        }
	    });

	}

	// Full Date Picker
  	selectFullDate();
    function selectFullDate() {
    	$("#extraCssUI").remove();
	    $('.select_full_date').focus(function(){
	    	$(".ui-datepicker-calendar").show();
	    }).datepicker({
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'dd / MM / yy',
	        create: function (input, inst) { 

	         },
	        onClose: function(dateText, inst) { 

	        }
	    });
	}

    // Add more Project Field
    $('#add_project').click( function(){
    	$("#add_more_project").append('<span class="more-field-top-border"> <div class="form-group col-md-6 col-sm-6"> <input type="text" class="form-control" name="project_title[]" placeholder="Project Title"> </div> <div class="form-group col-md-2 col-sm-2 "> <input type="text" class="form-control select_date" name="project_start[]" placeholder="Start Date"> </div> <div class="form-group col-md-2 col-sm-2 "> <input type="text" class="form-control select_date" name="project_end[]" placeholder="End Date"> </div><span class="swap-rows-mobile"><div class="form-group col-md-2 col-sm-2"> <i onclick="projectRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div> <div class="form-group col-md-12 col-sm-12"> <textarea type="text" class="form-control" name="project_des[]" rows="1" placeholder="Project Description: Tell us briefly about any type of projects you have successfully completed or are working on"></textarea> </div></span></span>');
    	selectDate();
    	selectFullDate();
		$(".select_date").attr("readonly","true");
		$("input.select_date").css("background-color","#fff");

		$(".select_full_date").attr("readonly","true");
		$("input.select_full_date").css("background-color","#fff");
		    });

    // Add more Award Field
    $('#add_award').click( function(){
    	$("#add_more_award").append('<span class="more-field-top-border"> <div class="form-group col-md-6 col-sm-6"> <input type="text" class="form-control" name="award_name[]" placeholder="Award Name"> </div> <div class="form-group col-md-4 col-sm-4 "> <input type="text" class="form-control select_full_date" name="award_date[]" placeholder="Award Date"> </div><span class="swap-rows-mobile"><div class="form-group col-md-2 col-sm-2"> <i onclick="projectRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div>  <div class="form-group col-md-12 col-sm-12"> <textarea type="text" class="form-control" name="award_des[]" rows="1" placeholder="Award Description: Tell us briefly about any type of awards you have received and why it was presented to you"></textarea> </div></span></span>');
    	selectDate();
    	selectFullDate();

		$(".select_date").attr("readonly","true");
		$("input.select_date").css("background-color","#fff");

		$(".select_full_date").attr("readonly","true");
		$("input.select_full_date").css("background-color","#fff");
    });

    // Add more Certificates Field
    $('#add_cert').click( function(){
    	$("#add_more_cert").append('<span class="more-field-top-border"> <div class="form-group col-md-4 col-sm-4"> <input type="text" class="form-control" name="cert_name[]" placeholder="Certificate Name"> </div> <div class="form-group col-md-4 col-sm-4"> <input type="text" class="form-control" name="issue_auth[]" placeholder="Issuing Authority"> </div> <div class="form-group col-md-4 col-sm-4 "> <input type="text" class="form-control select_full_date" name="cert_date[]" placeholder="Certificate Date"> </div><div class="form-group col-md-2 col-sm-2"> <i onclick="certRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div> </span>');
    	selectDate();
    	selectFullDate();

		$(".select_date").attr("readonly","true");
		$("input.select_date").css("background-color","#fff");

		$(".select_full_date").attr("readonly","true");
		$("input.select_full_date").css("background-color","#fff");
    });

    // Add more Work Experience
    $('#add_work').click( function(){
    	$("#add_more_work").append('<span class="more-field-top-border"><div class="form-group col-md-12 col-sm-12"> <input type="text" class="form-control" name="work_title[]" placeholder="Title"> </div><div class="form-group col-md-12 col-sm-12"> <input type="text" class="form-control" name="work_company[]" placeholder="Company Name"> </div> <div class="form-group col-md-12 col-sm-12"> <input type="text" class="form-control" name="work_location[]" placeholder="Location"> </div> <div class="form-group col-md-6 col-sm-6 "> <input type="text" class="form-control select_full_date" name="work_start[]" placeholder="Start Date"> </div> <div class="form-group col-md-6 col-sm-6 "> <input type="text" class="form-control select_full_date" name="work_end[]" placeholder="End Date"> </div><div class="form-group col-md-4 col-sm-4"> <i onclick="certRemove(this);" class="add-more-field fa fa-remove"><span class="label-desc"> Remove </span></i> </div> </span>');
    	selectDate();
    	selectFullDate();

		$(".select_date").attr("readonly","true");
		$("input.select_date").css("background-color","#fff");

		$(".select_full_date").attr("readonly","true");
		$("input.select_full_date").css("background-color","#fff");
    });

    // Remove Field
    function projectRemove(select) {
    	(((select.parentNode).parentNode)).parentNode.remove();
    }

    function certRemove(select) {
    	((select.parentNode).parentNode).remove();
    }

	$('#selectSchoolCity').on('change',selectSchoolCity);
  	// Begin select2 select
	select2Init();
  	function select2Init() {

  		getSkills();
  		getExtraActivities();
	    beforeSchoolCity();

	    $("#selectState").select2({
	        placeholder: "Select a state"
	    });

	    $("#selectCity").select2({
	        placeholder: "Select a City",
	        tags: true
	    });

	    $("#selectSchoolCity").select2({
	        placeholder: "Select School City",
	        tags: true
	    });

	    $("#selectSchool").select2({
	        placeholder: "Select School",
	        tags: true
	    });

		$("#high_school_subject").select2({
			placeholder: 'Type and press "enter" to add your own subjects/courses or choose from the suggestions.',
			// allowClear: true,
			width: "100%",
			tags: true
		});

		$("#gpa_un").select2({
			placeholder: "Select GPA"
		});

		$("#gpa_wei").select2({
			placeholder: "Select GPA Weighted"
		});


		$(".skils-select").select2({
			placeholder: 'Type and press "enter" to add your own skill or choose from the suggestions. Any quality or trait you have expertise in or are passionate about should be included here. List any skills that are not displayed elsewhere in your resume/profile.',
			// allowClear: true,
			tags: true
		});

		$(".extracurricular-activities-select").select2({
			placeholder: 'Type and press “enter” to add your own extracurricular activities. Please include any extracurricular activities that you wish to highlight to employers. This can include any sports, organizations, clubs on campus or any leadership positions that you are involved in.',
			// allowClear: true,
			tags: true
		});

	}

  // End select2 select

  	// onchange school city to get school
	function selectSchoolCity() {
		var selectSchoolCity = $("#selectSchoolCity").val();

        $.ajax({
            type:"post",
            url: "<?php echo base_url().'student/getSchools' ?>",
            data:{ selectSchoolCity : selectSchoolCity },
            success:function(response){
                //alert(response);
                $('#selectSchool').html(response);
                // $('#selectSchool').find('option').get(0).remove();
            }
        });
	}

  	// get skills when load and move on it's page 
	function getSkills() {

        $.ajax({
            type:"post",
            url: "<?php echo base_url().'student/getSkills' ?>",
            success:function(response){
                $('#skill').html(response);
            }
        });
	}

  	// get extra activities when load and move on it's page 
	function getExtraActivities() {

        $.ajax({
            type:"post",
            url: "<?php echo base_url().'student/getExtraActivities' ?>",
            success:function(response){
                $('#extra_activities').html(response);
            }
        });
	}

	// onchange state to get city
	function getCity() {
		var selectState = $("#selectState").val();
        
        $.ajax({
            type:"post",
            url: "<?php echo base_url().'student/getCity' ?>",
            data:{ selectState : selectState },
            success:function(response){
                //alert(response);
                $('#selectCity').html(response);
                $('#selectCity').find('option').get(0).remove();
                beforeSchoolCity();
            }
        });


	}

	// School city before saved

	function beforeSchoolCity() {
        var selectState = $("#selectState").val();
        // School City
        $.ajax({
            type:"post",
            url: "<?php echo base_url().'student/getSchoolCity' ?>",
            data:{ selectState : selectState },
            success:function(response){
                //alert(response);
                $('#selectSchoolCity').html(response);
                // $('#selectSchoolCity').find('option').get(0).remove();
                selectSchoolCity();
            }
        });

	}



</script>
