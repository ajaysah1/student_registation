<?php

namespace App\Controllers;

use App\Models\PersonalModel;

defined('BASEPATH') OR exit('No direct script access allowed');



class Student extends BaseController {

	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
		$this->load->helper('intermart');
		$this->load->model('front_model');
		$this->load->library('stripe_lib');
	}

	/**

	 * Index Page for this controller.

	 *

	 * Maps to the following URL

	 * 		http://example.com/index.php/welcome

	 *	- or -

	 * 		http://example.com/index.php/welcome/index

	 *	- or -

	 * Since this controller is set as the default controller in

	 * config/routes.php, it's displayed at http://example.com/

	 *

	 * So any other public methods not prefixed with an underscore will

	 * map to /index.php/welcome/<method_name>

	 * @see https://codeigniter.com/user_guide/general/urls.html

	 */

	public function index()

	{

		$this->load->view('Website/index');

	}

	public function signup()

	{
		if($this->session->userdata('userId') != '') {
			redirect('student/dashboard');
		}
		return redirect('student/account#signup');
		// $this->load->view('Website/signup',@$data);

	}

	public function email_temp()

	{


		$this->load->view('emails/forget_email',@$data);

	}

	public function changePassword($id)

	{

			if($this->input->post('email')){


				$p=$this->input->post('password');
				$cp=$this->input->post('cpassword');

		if($p==$cp){

			$data1 = array('password'=>$p);
			$this->db->where('email',$this->input->post('email'));
			$this->db->update('students',$data1);


			$this->db->select('*');
			$this->db->where('email',$this->input->post('email'));
			$query = $this->db->get('students');
			$res = $query->row();
			$data['name'] = $res->name;

			$to  =  $res->email;
			$message = $this->load->view('emails/password_change_email',$data,TRUE);
			$headers = 'From:noreply@internmart.com' . "\r\n";
			$subject =  $res->name.', your password was successfully reset';
			$sendmail=send_mails($to, $subject, $message, $headers);
    						if($sendmail){
    							$data['error']="<span style='color:red'>Password has been Changed.Please login</span>";
}

		}else{

			$data['error']="<span style='color:red'>Password & Confirm Password Not Matched</span>";

		}





	}


		$data['email'] = base64_decode($id);


		$this->load->view('Website/change_password',@$data);

	}


	public function forgetPassword()

	{

		if( $this->input->post('forget') == "forget" ) {
			$this->db->select('*');
			$this->db->where('email',$this->input->post('email'));
			$query = $this->db->get('students');
			//echo $this->db->last_query();die;
			if($query->num_rows() == 0) {
				$data['error']="<span style='color:red'>Email Not found in our record.</span>";
				//redirect('student/forgetPassword');
				$this->session->set_flashdata('error_msg_forget', '<span id="forgetMsg" style="position: relative; top: -7px; color:red">Email Not found in our record.</span>');
			}else{

				$res = $query->row();
				$data['name'] = $res->name;
				$data['email'] = $res->email;
				$to  =  $res->email;
				$message = $this->load->view('emails/forget_email',$data,TRUE);

				$headers = 'From:noreply@internmart.com' . "\r\n";
				$subject = 'Reset Your InternMart.com Password';
				$sendmail=send_mails($to, $subject, $message, $headers);
	    		if($sendmail) {
	    			$data['error']="<span style='color:green'>Email Sent to you. Please check and change your password.</span>";
	    			$this->session->set_flashdata('error_msg_forget', '<span id="forgetMsg" style="position: relative; top: -7px; color:green">Email Not found in our record.</span>');
				}else{
					$data['error']="<span style='color:red'>An unknown error occurred while sending the email</span>";
					//redirect('student/forgetPassword');
					$this->session->set_flashdata('error_msg_forget', '<span id="forgetMsg" style="position: relative; top: -7px; color:red">An unknown error occurred while sending the email</span>');
				}

			}

	}

		return redirect('student/account#forget');
		// $this->load->view('Website/forget_password',@$data);

	}


	public function signup_post()
	{
		if($this->input->post('register'))
		{
		$n=$this->input->post('name');
		$e=$this->input->post('email');
		$p=$this->input->post('password');
		$cp=$this->input->post('cpassword');

		if($p==$cp){
			$que=$this->db->query("select * from students where email='".$e."'");
			$row = $que->num_rows();
			if($row)
			{
			$data['error']="<span style='color:red'>This user already exists</span>";
			$this->session->set_flashdata('error_msg_signup', '<span id="signupMsg" style="position: relative; top: -7px; color:red">This user already exists</span>');
			}
			else
			{
			$que=$this->db->query("insert into students(name,email,password) values('$n','$e','$p')");
			$data['name'] = $n;
			$data['email'] = $e;
		    $to  =  $e;
			$message = $this->load->view('emails/verify_email',$data,TRUE);

			$headers = 'From:noreply@internmart.com' . "\r\n";
			$subject = 'Verify Your Email to Use InternMart.com';
			try{
				$sendmail=send_mails($to, $subject, $message, $headers);
			}catch( Exception $e) {

			}

				$this->db->select('*');
				$this->db->from('students');
				$this->db->where('email', $e);
				$result = $this->db->get()->result_array();

				$this->db->select('*');
				$this->db->from('student_subscription');
				$this->db->where('student_id', $result[0]['id']);
				$sub = $this->db->get()->result_array();
				if ( empty($sub) ) {
					$this->db->select('*');
					$this->db->from('subscription_master');
					$this->db->where('type', 'Free');
					$subs = $this->db->get()->result_array();
					if ( !empty($subs) ) {
						$insertData = array(
							"student_id" => $result[0]['id'],
							"subscription_id" => $subs[0]['subscription_id'],
							"subscription_startdate" => date('Y-m-d'),
							"subscription_enddate" => date('Y-m-d'),
							"creditpoints" => 0,
							"created_by" => $result[0]['id'],
							"created_on" => date('Y-m-d H:i:s')
						);
						$this->db->insert('student_subscription', $insertData);
					}
				}
			$this->session->set_flashdata('error_msg_signin', '<span id="signinMsg" style="color:green; position: relative; top: -5px;">Account has been successfully created.</span>');
			redirect('student/account#signin');

			/*//$data['error']="<h3 style='color:blue'>Your account created successfully</h3>";
			$this->session->set_userdata('isStudentLoggedIn',TRUE);
            $this->session->set_userdata('userId',$e);
            $this->session->set_userdata('userName',$n);
			redirect('student/profile'); */
			}

			}
			else{
			$data['error']="<span style='color:red'>Password & Confirm Password Not Matched</span>";
			$this->session->set_flashdata('error_msg_signup', '<span style="color:red">Password & Confirm Password Not Matched</span>');

		}
		}

		//redirect('student/signup',@$data);
		return redirect('student/account#signup');
		// $this->load->view('Website/signup',@$data);
	}

	public function verifyEmail($id){

		$email = base64_decode($id);
		$data1 = array('password'=>$p);
			$this->db->where('verify_email','1');
			$this->db->update('students',$data1);

			$this->db->select('*');
			$this->db->where('email',$email);
			$query = $this->db->get('students');
			$res= $query->row();

			$data['name'] = $res->name;
			$data['email'] = $res->email;
		    $to  =  $res->email;
			$message = $this->load->view('emails/welcome_email',$data,TRUE);

			$headers = 'From:noreply@internmart.com' . "\r\n";
			$subject = $res->name.', Welcome to InternMart.com';
			$sendmail=send_mails($to, $subject, $message, $headers);
    						if($sendmail){
    						$this->session->set_flashdata('success', 'Email Verify successfully. Please login');
						    redirect('student/login');

		}

	}


    /**
        ===========================================================
        Operation   :   check registration is completed or not
                    -----------------------------------------------
        Input       :
                    -----------------------------------------------
        Return      :   redirect to another page.
        ===========================================================
    **/

	public function checkReg() {

		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$reg_step = $data['users'][0]['reg_step'];
		// step check
		if( !isset($_GET['reg_step'])  ) {
			if( $reg_step <= 3 ) {
				if( $reg_step == 1 )
					redirect('student/registration?reg_step=1');
				if( $reg_step == 2 )
					redirect('student/registration?reg_step=2');
				if( $reg_step == 3 )
					redirect('student/registration?reg_step=3');
			}
		}

	}


    /**
        ===========================================================
        Operation   :   Sign In and Sign Up Function
                    -----------------------------------------------
        Input       :
                    -----------------------------------------------
        Return      :   Sign In & Sign Up Form
        ===========================================================
    **/

	public function account() {

		if($this->session->userdata('userId') != '') {
			redirect('student/dashboard');
		}

		$this->load->view('Website/student_account_login');
	}

    /**
        ===========================================================
        Operation   :   Interest Mapper
                    -----------------------------------------------
        Input       :
                    -----------------------------------------------
        Return      :   interestMapper View
        ===========================================================
    **/

	public function interest_mapper() {

		// $this->auth_login();
		if ( isset( $_GET['interest'] ) ) {

			if( $_GET['interest'] == 1 ) {
				return $this->load->view('Website/interestMapperStart1');
			}
		}

		return $this->load->view('Website/interestMapper');

	}

public function test()
{
echo view('step_registration');
}
    /**
        ===========================================================
        Operation   :   Registration pages
                    -----------------------------------------------
        Input       :   param url
                    -----------------------------------------------
        Return      :   registration page
        ===========================================================
    **/

	public function registration() {
		$this->auth_login();
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$student_id = $this->session->userdata('user_id');
		// get completed steps
		$reg_step = $data['users'][0]['reg_step'];
		$data['complete'] = $data['users'][0]['complete'];
		$data['reg_step'] = $reg_step;

		// Edit Profile
		if( isset($_GET['action'])  ) {
			if( $_GET['action'] == "reg_edit" ) {
				if( $reg_step <= 3 ) {
					if( $reg_step == 1 )
						redirect('student/registration?reg_step=1');
					if( $reg_step == 2 )
						redirect('student/registration?reg_step=2');
					if( $reg_step == 3 )
						redirect('student/registration?reg_step=3');
				}else{
					redirect('student/registration?reg_step=1');
				}
			}else{
				redirect('student/dashboardd');
			}
		}

		// step check
		if( !isset($_GET['reg_step'])  ) {
			if( $reg_step <= 3 ) {
				if( $reg_step == 1 )
					redirect('student/registration?reg_step=1');
				if( $reg_step == 2 )
					redirect('student/registration?reg_step=2');
				if( $reg_step == 3 )
					redirect('student/registration?reg_step=3');
			}else{
				redirect('student/dashboard');
			}
		}

		// State and city
		if( $data['users'][0]['city'] != 0 || $data['users'][0]['city'] != NULL ) {

			$this->db->select('*');
			$this->db->from('tbl_city');
			$this->db->where('id', $data['users'][0]['city'] );
			$state = $this->db->get()->result_array();

			$data['s_city'] = $data['users'][0]['city'];
			if( !empty($state) ) {
				if( $data['s_city'] != 0 ) {
						$data['city'] = $this->front_model->getCity($state[0]['state_id']);
						$data['s_state'] = $state[0]['state_id'];
				}
			}
		}

		// Mobile Number
		if( $data['users'][0]['mobile'] != 0 || $data['users'][0]['mobile'] != NULL ) {
			$data['s_mobile'] = $data['users'][0]['mobile'];
		}

		// Parent Mail
		if( $data['users'][0]['par_mail'] != 0 || $data['users'][0]['par_mail'] != NULL ) {
			$data['par_mail'] = $data['users'][0]['par_mail'];
		}

		// Blog
		if( $data['users'][0]['blog'] != 0 || $data['users'][0]['blog'] != NULL ) {
			$data['blog'] = $data['users'][0]['blog'];
		}

		// Insta
		if( $data['users'][0]['insta'] != 0 || $data['users'][0]['insta'] != NULL ) {
			$data['insta'] = $data['users'][0]['insta'];
		}

		// Website
		if( $data['users'][0]['website'] != 0 || $data['users'][0]['website'] != NULL ) {
			$data['website'] = $data['users'][0]['website'];
		}

		// LinkedIn
		if( $data['users'][0]['linkedin'] != 0 || $data['users'][0]['linkedin'] != NULL ) {
			$data['linkedin'] = $data['users'][0]['linkedin'];
		}

		// Student Subjects
		$simple_array_sub = getStudentSubjectHelper($this->session->userdata('user_id'));
		$select_subs = array();
		foreach ($simple_array_sub as $key) {
			array_push($select_subs, $key['subject_id']);
		}
		$data['student_subject'] = $select_subs;
		$data['subject'] = getSubjectHelper();

		// GPA UnWeighted
		if( $data['users'][0]['gpa'] != 0 || $data['users'][0]['gpa'] != NULL ) {
			$data['gpa'] = $data['users'][0]['gpa'];
		}

		// GPA Weighted
		if( $data['users'][0]['gpa_wei'] != 0 || $data['users'][0]['gpa_wei'] != NULL ) {
			$data['gpa_wei'] = $data['users'][0]['gpa_wei'];
		}

		// Awards, Project, Certificate
		$data['student_project'] = getStudentProjectHelper($student_id);
		$data['student_award'] = getStudentAwardHelper($student_id);
		$data['student_cert'] = getStudentCertHelper($student_id);

		// Work Experience
		$data['student_work_exp'] = getStudentWorkHelper($student_id);

		// Student Goals
		if( $data['users'][0]['career_goals'] != 0 || $data['users'][0]['career_goals'] != NULL ) {
			$data['career_goals'] = $data['users'][0]['career_goals'];
		}

		// Student Resume
		if( $data['users'][0]['resume'] != 0 || $data['users'][0]['resume'] != NULL ) {
			$data['resume'] = $data['users'][0]['resume'];
		}


		$data['states'] = getStatesHelper();

		$this->load->view('Website/step_registration', $data);
	}

    /**
        ===========================================================
        Operation   :   get city
                    -----------------------------------------------
        Input       :   state id using post service
                    -----------------------------------------------
        Return      :	list of city
        ===========================================================
    **/

	public function getCity() {
		$id=$this->input->post('selectState');
		$citys = $this->front_model->getCity($id);
		//print_r($student);die;
		// echo '<option></option>';
		foreach($citys as $city){
			echo '<option value="'. $city['id'] .'">'.$city['name'].'</option>';
		}
	}

    /**
        ===========================================================
        Operation   :   get schools city
                    -----------------------------------------------
        Input       :   state id using post service
                    -----------------------------------------------
        Return      :	list of city
        ===========================================================
    **/

	public function getSchoolCity() {
		$this->auth_login();
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$id=$this->input->post('selectState');
		$citys = $this->front_model->getCity($id);

		if( $data['users'][0]['school_city'] != 0 || $data['users'][0]['school_city'] != NULL ) {
			$data['school_city'] = $data['users'][0]['school_city'];

			foreach($citys as $city) {
				if ( $city['id'] == $data['users'][0]['school_city'] ) {
					$selected = "selected ";
				}else{
					$selected = "";
				}
				echo '<option '.$selected.'value="'. $city['id'] .'">'.$city['name'].'</option>';
			}

		}else{
			foreach($citys as $city){
				echo '<option value="'. $city['id'] .'">'.$city['name'].'</option>';
			}
		}
	}

    /**
        ===========================================================
        Operation   :   Get School
                    -----------------------------------------------
        Input       :   School City ID using post service
                    -----------------------------------------------
        Return      :	List of schools
        ===========================================================
    **/

	public function getSchools() {
		$this->auth_login();
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$id=$this->input->post('selectSchoolCity');
		$schools = $this->front_model->getTblSchool($id);

		if( $data['users'][0]['school_id'] != 0 || $data['users'][0]['school_id'] != NULL ) {

			$data['s_school'] = $this->front_model->getTblSchool($data['users'][0]['school_city']);

			$data['school_id'] = $data['users'][0]['school_id'];

			foreach($schools as $school) {
				if ( $school['id'] == $data['school_id'] ) {
					$selected = "selected ";
				}else{
					$selected = "";
				}
				echo '<option '.$selected.'value="'. $school['id'] .'">'.$school['name'].'</option>';
			}
		}else{
			foreach($schools as $school) {
				echo '<option value="'. $school['id'] .'">'.$school['name'].'</option>';
			}
		}
	}

    /**
        ===========================================================
        Operation   :   Get Skills
                    -----------------------------------------------
        Input       :
                    -----------------------------------------------
        Return      :	data in array
        ===========================================================
    **/

	public function getSkills() {
		$this->auth_login();
		$student_id = $this->session->userdata('user_id');
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));

		$skills = $this->front_model->getSkill();
		$student_skill = $this->front_model->getStudentSkillModel($student_id);
		$student_skill_array = array();
		foreach ($student_skill as $key) {
			array_push($student_skill_array, $key['skill_id']);
		}

		if( !empty($student_skill) ) {

			foreach($skills as $row) {
				if ( in_array($row['id'], $student_skill_array) ) {
					$selected = "selected ";
				}else{
					$selected = "";
				}
				echo '<option '.$selected.'value="'. $row['id'] .'">'.$row['name'].'</option>';
			}

		}else{
			foreach($skills as $row) {
				echo '<option value="'. $row['id'] .'">'.$row['name'].'</option>';
			}
		}
	}

    /**
        ===========================================================
        Operation   :   Get Extra Activities
                    -----------------------------------------------
        Input       :
                    -----------------------------------------------
        Return      :	data in array
        ===========================================================
    **/

	public function getExtraActivities() {
		$this->auth_login();
		$student_id = $this->session->userdata('user_id');
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));

		$extraActivities = $this->front_model->getExtraActivitiesModel();
		$studentExtraActivities = $this->front_model->studentExtraActivitiesModel($student_id);
		$student_extra_array = array();
		foreach ($studentExtraActivities as $key) {
			array_push($student_extra_array, $key['extra_activities_id']);
		}

		if( !empty($studentExtraActivities) ) {

			foreach($extraActivities as $row) {
				if ( in_array($row['id'], $student_extra_array) ) {
					$selected = "selected ";
				}else{
					$selected = "";
				}
				echo '<option '.$selected.'value="'. $row['id'] .'">'.$row['name'].'</option>';
			}

		}else{
			foreach($extraActivities as $row) {
				echo '<option value="'. $row['id'] .'">'.$row['name'].'</option>';
			}
		}
	}


    /**
        ===========================================================
        Operation   :   Save Registration Data
                    -----------------------------------------------
        Input       :   step position
                    -----------------------------------------------
        Return      :
        ===========================================================
    **/

	public function savereg() {
		$student_id = $this->session->userdata('user_id');
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		// Step 1
		if( $_GET['save_step'] == '1' ) {
			$postData = $this->input->post();

			// Begin Check new City Enter or not

				if ( is_numeric($postData['city']) ) {
				}else{
					$newCity = array(
						"name" => $postData['city'],
						"state_id" => $postData['state'],
						"insert_by" => "user"
					);
					$this->db->insert('tbl_city', $newCity);
					$postData['city'] = $this->db->insert_id();
				}

			// End Check new City Enter or not

			$this->db->select('*');
			$this->db->from('tbl_city');
			$this->db->where('id', $postData['city']);
			$city = $this->db->get()->result_array();
			if($data['users'][0]['reg_step'] > 2) {
				$reg_step = $data['users'][0]['reg_step'];
			}else{
				$reg_step = 2;
			}
			$insertData = array(
				"city" => $postData['city'],
				"city_display" => $city[0]['name'],
				"mobile" => $postData['s_mobile'],
				"par_mail" => $postData['p_email'],
				"insta" => $postData['s_instagram'],
				"website" => $postData['s_website'],
				"linkedin" => $postData['s_linkedin'],
				"blog" => $postData['s_blog'],
				"reg_step" => $reg_step

			);

			$this->db->where('id', $student_id);
			$this->db->update('students', $insertData);
			return print_r("1");
		}

		if( $_GET['save_step'] == '2' ) {
			if($data['users'][0]['reg_step'] > 3) {
				$reg_step = $data['users'][0]['reg_step'];
			}else{
				$reg_step = 3;
			}
			$postData = $this->input->post();
			// transaction start
			$this->db->trans_start();

			// Begin Check new City Enter or not

				if ( is_numeric($postData['school_city']) ) {
				}else{

					$this->db->select('*');
					$this->db->from('tbl_city');
					$this->db->where('id', $data['users'][0]['city'] );
					$getState = $this->db->get()->result_array();

					$newCity = array(
						"name" => $postData['school_city'],
						"state_id" => $getState[0]['state_id'],
						"insert_by" => "user"
					);
					$this->db->insert('tbl_city', $newCity);
					$postData['school_city'] = $this->db->insert_id();
				}

			// End Check new City Enter or not

			// Begin Check new School Enter or not

				if ( is_numeric($postData['s_school']) ) {
				}else{

					$this->db->select('*');
					$this->db->from('tbl_city');
					$this->db->where('id', $postData['school_city'] );
					$getState = $this->db->get()->result_array();

					$this->db->select('*');
					$this->db->from('tbl_state');
					$this->db->where('id', $getState[0]['state_id'] );
					$getStateName = $this->db->get()->result_array();

					$newCity = array(
						"city_id" => $postData['school_city'],
						"name" => $postData['s_school'],
						"city" => $getState[0]['name'],
						"state" => $getStateName[0]['name'],
						"insert_by" => "user"
					);
					$this->db->insert('tbl_schools', $newCity);
					$postData['s_school'] = $this->db->insert_id();
				}

			// End Check new School Enter or not

			$this->db->select('*');
			$this->db->from('tbl_city');
			$this->db->where('id', $postData['school_city']);
			$city = $this->db->get()->result_array();

			$this->db->select('*');
			$this->db->from('tbl_schools');
			$this->db->where('id', $postData['s_school']);
			$school = $this->db->get()->result_array();

			$insertData = array(
				"school_city" => $postData['school_city'],
				"school_city_display" => $city[0]['name'],
				"school_id" => $postData['s_school'],
				"school_name" => $school[0]['name'],
				"gpa" => $postData['gpa_un'],
				"gpa_wei" => $postData['gpa_wei'],
				"reg_step" => $reg_step
			);

			$this->db->where('id', $student_id);
			$this->db->update('students', $insertData);

			// Update, Insert and delete Student Subjects.

				// Begin Check New Subject add or not
				$finalSubjects = array();
				foreach ($postData['high_school_subject'] as $key) {

					if ( is_numeric($key) ) {
						$finalSubjects[] = $key;
					}else{
						$newSubject = array(
							"name" => $key,
							"status" => 1,
							"insert_by" => "user"
						);
						$this->db->insert('high_school_subject', $newSubject);
						$finalSubjects[] = $this->db->insert_id();
					}

				}
				$postData['high_school_subject'] = $finalSubjects;
				// End Check New Subject add or not

			$countPost = count($postData['high_school_subject']);
			$this->db->select('*');
			$this->db->from('student_subject');
			$this->db->where('student_id', $student_id);
			$student_subject = $this->db->get()->result_array();
			$countDb = count($student_subject);

			if( $countDb == 0 ){
				$i=0;
				foreach ($postData['high_school_subject'] as $key ) {
					$sub = array(
						"student_id" => $student_id,
						"subject_id" => $key[$i]
					);
					$this->db->insert('student_subject', $sub);
					$sub = NULL;
				}
			}

			if( $countDb != 0 ){
				$i = 0;
				if ( $countDb > $countPost ) {
					for ($i=0; $i < $countPost; $i++) {
						$sub = array(
							"student_id" => $student_id,
							"subject_id" => $postData['high_school_subject'][$i]
						);
						$student_subject_id = $student_subject[$i]['id'];
						$this->db->where('id', $student_subject_id);
						$this->db->update('student_subject', $sub);

					}
					for ($j=$i; $j < $countDb; $j++) {
						$student_subject_id = $student_subject[$j]['id'];
						$this->db->where('id', $student_subject_id);
						$this->db->delete('student_subject');
					}
				}
				if ( $countDb < $countPost ) {
					for ($i=0; $i < $countDb; $i++) {
						$sub = array(
							"student_id" => $student_id,
							"subject_id" => $postData['high_school_subject'][$i]
						);
						$student_subject_id = $student_subject[$i]['id'];
						$this->db->where('id', $student_subject_id);
						$this->db->update('student_subject', $sub);

					}
					for ($j=$i; $j < $countPost; $j++) {
						$sub = array(
							"student_id" => $student_id,
							"subject_id" => $postData['high_school_subject'][$j]
						);
						$this->db->insert('student_subject', $sub);
					}
				}

			}
			// transaction end
			$this->db->trans_complete();

			if ($this->db->trans_status() === FALSE) {
			    return print_r("0");
			}else{
				return print_r("1");
			}

		}

		if( $_GET['save_step'] == '3' ) {

			if($data['users'][0]['reg_step'] > 4) {
				$reg_step = $data['users'][0]['reg_step'];
			}else{
				$reg_step = 4;
			}
			$postData = $this->input->post();
			$this->db->trans_start();

			$insertData = array(
				"reg_step" => $reg_step
			);
			$this->db->where('id', $student_id);
			$this->db->update('students', $insertData);
			// ******* Skills ********
			$i = 0;
			$this->db->where('student_id', $student_id);
			$this->db->delete('student_skill');

			foreach ($postData['skill'] as $key) {

				if ( is_numeric($key) ) {
					$insertSkill = array(
						"student_id" => $student_id,
						"skill_id" => $key
					);
					$this->db->insert('student_skill', $insertSkill);
				}else{
					$newSkill = array(
						"name" => $key,
						"status" => 1,
						"insert_by" => "user"
					);
					$this->db->insert('skills', $newSkill);

					$insertSkill = array(
						"student_id" => $student_id,
						"skill_id" => $this->db->insert_id()
					);
					$this->db->insert('student_skill', $insertSkill);
				}

			}
			// ******* Extra Activities ********
			$i = 0;
			$this->db->where('student_id', $student_id);
			$this->db->delete('student_extra_activities');

			foreach ($postData['extra_activities'] as $key) {

				if ( is_numeric($key) ) {
					$insertExtraAct = array(
						"student_id" => $student_id,
						"extra_activities_id" => $key
					);
					$this->db->insert('student_extra_activities', $insertExtraAct);
				}else{
					$newExtra = array(
						"name" => $key,
						"status" => 1,
						"insert_by" => "user"
					);
					$this->db->insert('extra_activities', $newExtra);

					$insertExtraAct = array(
						"student_id" => $student_id,
						"extra_activities_id" => $this->db->insert_id()
					);
					$this->db->insert('student_extra_activities', $insertExtraAct);
				}

			}
			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
			    return print_r("0");
			}else{
				return print_r("1");
			}
		}

		if( $_GET['save_step'] == '4' ) {
			if($data['users'][0]['reg_step'] > 4) {
				$reg_step = $data['users'][0]['reg_step'];
			}else{
				$reg_step = 4;
			}
			$postData = $this->input->post();
			$this->db->trans_start();

			$insertData = array(
				"reg_step" => $reg_step
			);
			$this->db->where('id', $student_id);
			$this->db->update('students', $insertData);

			// Delete before insert.
			$this->db->where('student_id', $student_id);
			$this->db->delete('student_project');
			$this->db->where('student_id', $student_id);
			$this->db->delete('student_award');
			$this->db->where('student_id', $student_id);
			$this->db->delete('student_cert');
			// Insert Project
			$project_count = count( $postData['project_title'] );
			for ($i=0; $i < $project_count; $i++) {
				$projectDataInsert = array(
					'student_id' => $student_id,
					'project_title' => $postData['project_title'][$i],
					'start_date' => $postData['project_start'][$i],
					'end_date' => $postData['project_end'][$i],
					'pro_desc' => $postData['project_des'][$i]
				);
				$this->db->insert('student_project', $projectDataInsert);
			}

			// Insert Awards
			$award_count = count( $postData['award_name'] );
			for ($i=0; $i < $award_count; $i++) {
				$awardDataInsert = array(
					'student_id' => $student_id,
					'award_title' => $postData['award_name'][$i],
					'award_date' => $postData['award_date'][$i],
					'award_desc' => $postData['award_des'][$i]
				);
				$this->db->insert('student_award', $awardDataInsert);
			}

			// Insert Certificate
			$cert_count = count( $postData['cert_name'] );
			for ($i=0; $i < $cert_count; $i++) {
				$projectDataInsert = array(
					'student_id' => $student_id,
					'cert_name' => $postData['cert_name'][$i],
					'issue_auth' => $postData['issue_auth'][$i],
					'cert_date' => $postData['cert_date'][$i]
				);
				$this->db->insert('student_cert', $projectDataInsert);
			}

			$this->db->trans_complete();
			if ($this->db->trans_status() === FALSE) {
			    return print_r("0");
			}else{
				return print_r("1");
			}
		}

		if( $_GET['save_step'] == '5' ) {

			$reg_step = 5;
			$postData = $this->input->post();

			$this->db->trans_start();

				$insertData = array(
					"reg_step" => $reg_step,
					"career_goals" => $postData['career_goals'],
					"complete" => 1
				);
				$this->db->where('id', $student_id);
				$this->db->update('students', $insertData);

				// Delete before insert.
				$this->db->where('student_id', $student_id);
				$this->db->delete('student_work_exp');

				// Insert Work
				$work_count = count( $postData['work_title'] );
				for ($i=0; $i < $work_count; $i++) {
					$workDataInsert = array(
						'student_id' => $student_id,
						'work_title' => $postData['work_title'][$i],
						'company_name' => $postData['work_company'][$i],
						'location' => $postData['work_location'][$i],
						'start_date' => $postData['work_start'][$i],
						'end_date' => $postData['work_end'][$i]
					);
					$this->db->insert('student_work_exp', $workDataInsert);
				}


			$this->db->trans_complete();

			if ( $_FILES["file_resume"]["name"] != "" ) {
		        $config['upload_path'] = RESUME_FILE_PATH;
		        $config['allowed_types'] = RESUME_FILE_FORMAT;
		        $config['max_size'] = 3000;
		        $new_name = time().$_FILES["file_resume"]['name'];
		        $config['file_name'] = $new_name;
		        $this->load->library('upload', $config);
		        $this->upload->initialize($config);

		        if (!$this->upload->do_upload('file_resume')) {
		            $error = $this->upload->display_errors();
		            $this->session->set_flashdata('error_msg', $error);
		        	return redirect('student/registration?reg_step=5');
		        } else {
		            $getFileData = $this->upload->data();
		            $resume_name = $getFileData['file_name'];

					$insertData = array(
						"resume" => $resume_name
					);
					$this->db->where('id', $student_id);
					$this->db->update('students', $insertData);

		        }
		    }

			if ($this->db->trans_status() === FALSE) {
			    return redirect('student/registration?reg_step=5');
			}else{

				$this->session->set_flashdata('success', 'Updated successfully');

				if ( empty($postData['redir_value']) ) {
					$checkRedir = "";
				}else{
					$checkRedir = $postData['redir_value'];
				}

				if( $checkRedir == "career" ) {
					return redirect('career');
				}elseif( $checkRedir == "cancel" ){
					return redirect('student/dashboard');
				}else{
					return redirect('student/dashboard');
				}
			}

		}

	}

	public function login()

	{

		//$this->load->view('Admin/login');
		if($this->session->userdata('userId') != '') {
			redirect('student/dashboard');
		}

		redirect('student/account#signin');
		// $this->load->view('Website/login');

	}



	public function saveIntership()

	{

		//$this->load->view('Admin/login');

		$this->db->select('jobs.*,tbl_job_apply.*');

		$this->db->join('tbl_job_apply', 'tbl_job_apply.job_id = jobs.id');

		$this->db->where('tbl_job_apply.user_id',$this->session->userdata('userId'));
		$this->db->order_by('tbl_job_apply.create_date','DESC');
		//$this->db->limit('8');

		$query = $this->db->get('jobs');

		$data['result3'] = $query->result();

		$this->load->view('Website/saved-internships');

	}

	public function matchIntership()

	{
		$this->auth_login();
		$this->checkReg();
		//$this->load->view('Admin/login');
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));


		$this->db->where('tbl_job_apply.user_id',$this->session->userdata('user_id'));

		$query = $this->db->get('tbl_job_apply');
//echo $this->db->last_query();
		$apjob = $query->result();


		$jobId = array();

		if(!empty($apjob) && count($apjob) >0 ) {
			foreach($apjob as $job) {
			$jobId[] = $job->job_id;
			}

			$this->db->where_not_in('id', $jobId);

		}

		// $jobId = array();

		// foreach($apjob as $job) {
		// 	$jobId[] = $job->job_id;
		// }
		// $this->db->where_not_in('id', $jobId);

		$this->db->order_by('rand()');

		$this->db->limit('8');

		$query = $this->db->get('jobs');

		$data['result3'] = $query->result();
		$this->load->view('Website/match-internships',$data);

	}

	public function appliedIntership()

	{
		$this->auth_login();
		$this->checkReg();
		$this->db->select('jobs.*,tbl_job_apply.*');

		$this->db->join('tbl_job_apply', 'tbl_job_apply.job_id = jobs.id');

		$this->db->where('tbl_job_apply.user_id',$this->session->userdata('user_id'));
		$this->db->order_by('tbl_job_apply.create_date','DESC');
		//$this->db->limit('8');

		$query = $this->db->get('jobs');
//echo $this->db->last_query();
		$data['result3'] = $query->result();

		//$this->load->view('Admin/login');
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$this->load->view('Website/applied-internships',$data);

	}

	public function knowledgeCenter()

	{

		//$this->load->view('Admin/login');

		$this->load->view('Website/knowledge-center');

	}

	public function login_chk(){
		if($this->input->post('login'))
		{

			$e=$this->input->post('email');
			$p=$this->input->post('password');

			$que=$this->db->query("select * from students where email='".$e."' and password ='".$p."'");
			$row = $que->num_rows();
			if($row) {

				$result = $que->row_array();

				// Check free subscription
				$this->db->select('*');
				$this->db->from('student_subscription');
				$this->db->where('student_id', $result['id']);
				$sub = $this->db->get()->result_array();
				if ( empty($sub) ) {
					$this->db->select('*');
					$this->db->from('subscription_master');
					$this->db->where('type', 'Free');
					$subs = $this->db->get()->result_array();
					if ( !empty($subs) ) {
						$insertData = array(
							"student_id" => $result['id'],
							"subscription_id" => $subs[0]['subscription_id'],
							"subscription_startdate" => date('Y-m-d'),
							"subscription_enddate" => date('Y-m-d'),
							"creditpoints" => 0,
							"created_by" => $result['id'],
							"created_on" => date('Y-m-d H:i:s')
						);
						$this->db->insert('student_subscription', $insertData);
					}
				}

				$this->session->set_userdata('isStudentLoggedIn',TRUE);
	            $this->session->set_userdata('userId',$e);
	            $this->session->set_userdata('user_id',$result['id']);
	            $this->session->set_userdata('userName',$result['name']);
	            //print_r($result);
				$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
				// get completed steps
				$reg_step = $data['users'][0]['reg_step'];
				if( $reg_step <= 3 ) {
					redirect('student/registration');
				}else{
					redirect('student/dashboard');
				}
	    //         if($result['name'] != "" && $result['email'] != "" && $result['city'] != "" && $result['mobile'] != "" && $result['objective'] != "" && $result['school_city'] != "" && $result['school_name'] != "" && $result['grade'] != "" && $result['gpa'] != "" && $result['electives'] != "" && $result['skills'] != "" && $result['sub_electives'] != ""){
	    //         	redirect('student/dashboard');
	    //         }
	    //         else{

					// redirect('student/profile');
	    //         }

			}
			else {
				$data['error']="<span style='color:red'>Wrong User Password</span>";
				$this->session->set_flashdata('error_msg_signin', '<span id="signinMsg" style="position: relative; top: -7px; color:red">Wrong User Password</span>');
				// $this->load->view('Website/login',@$data);
			}

		}
		// $this->load->view('Website/login',@$data);
		redirect('student/account#signin');
	}

	public function profile()

	{

		$this->auth_login();
		$this->checkReg();
		redirect('student/registration?action=reg_edit');
		$data = array();

		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));

		$data['result'] = $this->Student_model->first_level();
		$data['result2'] = $this->Student_model->second_level();

		$data['result3'] = $this->Student_model->skill_level();

		$data['city'] = $this->Student_model->get_school_city();
		//print_r($this->session->userdata('userId')); die;
		$this->load->view('Website/manage-profile',@$data);

	}

	public function dashboard()

	{

		$this->auth_login();
		$this->checkReg();

		$data = array();
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$data['result'] = $this->Student_model->first_level();
		$data['result2'] = $this->Student_model->second_level();
		$data['city'] = $this->Student_model->get_school_city();


		$this->db->where('tbl_job_apply.user_id',$this->session->userdata('user_id'));

		$query = $this->db->get('tbl_job_apply');
//echo $this->db->last_query();
		$apjob = $query->result();

		$jobId = array();

		foreach($apjob as $job) {
			$jobId[] = $job->job_id;
		}

if(!empty($jobId)) {

		$this->db->where_not_in('id', $jobId);

		$this->db->order_by('rand()');

		$this->db->limit('8');

		$query = $this->db->get('jobs');
			$data['result3'] = $query->result();
}else{

		$data['result3'] = '';
}

		$this->load->view('Website/dashboard',@$data);

	}

	/**
		===========================================================
		Operation	:	Check Quiz
					-----------------------------------------------
		Input 		:	User ID
					-----------------------------------------------
		Return 		:	quiz page if true.
		===========================================================
	**/

	public function takeQuiz() {
		$this->auth_login();
		$this->checkReg();
		$data = $this->Student_model->my_profile($this->session->userdata('userId'));

		checkQuiz($data[0]['id']);
	}

	/**
		===========================================================
		Operation	:	Show Subscription status and renew.
					-----------------------------------------------
		Input 		:	Optional: pay or history.
					-----------------------------------------------
		Return 		:	Subscription View page
		===========================================================
	**/

	public function subscription($checkReq="", $subs_id="")

	{

		$this->auth_login();
		$this->checkReg();

		$data = array();
		$data['users'] = $this->Student_model->my_profile($this->session->userdata('userId'));
		$data['result'] = $this->Student_model->first_level();
		$data['result2'] = $this->Student_model->second_level();
		$data['city'] = $this->Student_model->get_school_city();
		$data['subscription_master'] = getSubscriptionPlan();
		$data['my_subscription'] = getMySubscription($data['users'][0]['id']);
		// print_r($data['my_subscription']);
		// die();
		$data['price_feature'] = getPlanFeature();
		$data['plans'] = getPlans();

		$this->db->where('tbl_job_apply.user_id',$this->session->userdata('user_id'));

		$query = $this->db->get('tbl_job_apply');
		$apjob = $query->result();
		$jobId = array();

		foreach($apjob as $job) {
			$jobId[] = $job->job_id;
		}

		if(!empty($jobId)) {

			$this->db->where_not_in('id', $jobId);
			$this->db->order_by('rand()');
			$this->db->limit('8');
			$query = $this->db->get('jobs');

			$data['result3'] = $query->result();
		}else{

			$data['result3'] = '';
		}

		// View History
		if($checkReq == "history") {
			$data['subs_history'] = getStuSubsHistory($data['users'][0]['id']);
			foreach ( $data['subs_history'] as &$key ) {
				$key['encPaymentId'] = $this->front_model->encryptdata($key['payment_id']);
			}

			return $this->load->view('Website/subsHistory',@$data);
		}

		// View Receipt
		if($checkReq == "viewReceipt") {
			$data['subs_history'] = getStuSubsHistory($data['users'][0]['id']);
			$data['payment_id'] = $this->front_model->decryptdata($subs_id);
			$this->db->select('*');
			$this->db->from('student_subs_payment');
			$this->db->where('payment_id', $data['payment_id']);
			$receiptdata = $this->db->get()->result_array();
			$data['response'] = json_decode($receiptdata[0]['charge']);
			// print_r($data['response']);
			// die();
			return $this->load->view('emails/payment_email',@$data);
		}
		// Download Receipt
		if($checkReq == "downReceipt") {

			$data['subs_history'] = getStuSubsHistory($data['users'][0]['id']);
			$data['payment_id'] = $this->front_model->decryptdata($subs_id);
			$this->db->select('*');
			$this->db->from('student_subs_payment');
			$this->db->where('payment_id', $data['payment_id']);
			$receiptdata = $this->db->get()->result_array();
			$data['response'] = json_decode($receiptdata[0]['charge']);
			$data['print'] = "1";
			// print_r($data);
			// die();
			return $this->load->view('emails/payment_email',$data);
		}

		if($checkReq == "pay") {
			$desubs_id = $this->front_model->decryptdata($subs_id);
			$subs_details = getSubscriptionPlan($desubs_id);
			$data['amount'] = $subs_details[0]->amount;
			$data['subs_id'] = $subs_id;

			// If payment form is submitted with token
	        if($this->input->post('stripeToken')){
	            // Retrieve stripe token, card and user info from the submitted form data
	            $postData = $this->input->post();
	            $postData['product'] = $subs_details;

	            $beforePaymentData = array(
	            	"student_id" => $data['users'][0]['id'],
	            	"subscription_id" => $desubs_id,
	            	"amount" => $subs_details[0]->amount,
	            	"currency_type" => STRIPE_CURRENCY,
	            	"provider" => PAYMENT_PROVIDER,
	            	"created_by" => $data['users'][0]['id'],
	            	"created_on" => date('Y-m-d H:i:s')
	            );
	            $postData['order_id'] = beforePaymentInsert($beforePaymentData);
	            // $postData['customer_id'] = $data['users'][0]['id'];
	            $postData['email'] = $data['users'][0]['email'];
	            $postData['user_id'] = $data['users'][0]['id'];

	            // Make payment
	            $status = $this->payment($postData);
	            // If payment successful
	            if($status == "succeeded") {
	            	$previousSubs = $data['my_subscription'];
	            	insertStuSubsAndHistory($postData['order_id'], $subs_details, $previousSubs, $data['users'][0]['id']);

	            	// Email
					// $data['payment_id'] = $postData['order_id'];
					// $this->db->select('*');
					// $this->db->from('student_subs_payment');
					// $this->db->where('payment_id', $data['payment_id']);
					// $receiptdata = $this->db->get()->result_array();
					// $data['response'] = json_decode($receiptdata[0]['charge']);
	    //         	receiptMail($data, $postData['email']);

	            	$this->session->set_flashdata('success', 'Payment Completed Successfully');
	                redirect('student/subscription/');
	            }else{
	                $apiError = !empty($this->stripe_lib->api_error)?' ('.$this->stripe_lib->api_error.')':'';
	                $data['error_msg'] = 'Transaction has been failed!'.$apiError;
	                $this->session->set_flashdata('error_msg', $data['error_msg']);
	            }
	        }

			return $this->load->view('Website/subsPay',@$data);
		}

		return $this->load->view('Website/subscription',@$data);

	}


	private function payment($postData) {

        // If post data is not empty
        if(!empty($postData)){
            // Retrieve stripe token, card and user info from the submitted form data
            $token  = $postData['stripeToken'];
            $name = $postData['name'];
            $email = $postData['email'];
            $card_number = $postData['card_number'];
            $card_number = preg_replace('/\s+/', '', $card_number);
            $card_exp_month = $postData['card_exp_month'];
            $card_exp_year = $postData['card_exp_year'];
            $card_cvc = $postData['card_cvc'];

            // Unique order ID
            $orderID = strtoupper(str_replace('.','',uniqid('', true)));

            // Add customer to stripe
            $checkCustomer = knowCustomerId($postData['user_id']);
            if( $checkCustomer == "0" ) {
            	$customer = $this->stripe_lib->addCustomer($email, $token);
            }else{
				$customer = new \StdClass();
				$customer->id = $checkCustomer;
            }

            if($customer){
                // Charge a credit or a debit card
                $charge = $this->stripe_lib->createCharge($customer->id,  $postData['product'][0]->name,  $postData['product'][0]->amount, $postData['order_id'], $email);

                // Insert response
                $chargeResponse = array(
                	"charge" => json_encode($charge)
                );
                afterPaymentInsert( $chargeResponse, $postData['order_id'] );

                if($charge){

                    // Check whether the charge is successful
                    if($charge['amount_refunded'] == 0 && empty($charge['failure_code']) && $charge['paid'] == 1 && $charge['captured'] == 1){
                        // Transaction details
                        $transactionID = $charge['balance_transaction'];
                        $paidAmount = $charge['amount'];
                        $paidAmount = ($paidAmount/100);
                        $paidCurrency = $charge['currency'];
                        $payment_status = $charge['status'];


                        // Insert tansaction data into the database
                        $afterPaymentUpdate = array(
                        	"transaction_id" => $transactionID,
                        	"customer_id" => $customer->id,
                        	"payment_status" =>  $charge['status'],
                        	"date_of_payment" => date('Y-m-d H:i:s'),
                        	"charge" => json_encode($charge)
                        );

                        afterPaymentInsert( $afterPaymentUpdate, $postData['order_id'] );
                        return $payment_status;
                    }
                }
            }
        }
        return false;
    }


	/**
		===========================================================
		Operation	:	Show pricing list.
					-----------------------------------------------
		Input 		:
					-----------------------------------------------
		Return 		:	Pricing list
		===========================================================
	**/

	public function price() {
		$this->auth_login();
		$this->checkReg();

		$data = array();
		$data['price_feature'] = getPlanFeature();
		$data['plans'] = getPlans();
		// print_r($data['plans']);
		// die();
		$this->load->view('Website/price',$data);
	}

	public function second_level(){


			$id = join("','", $this->input->post("selected"));

$query = $this->db->query("SELECT * FROM sub_electives
 WHERE elective_id IN ('".$id."')");
            $result = $query->result_array();
 $output = '';
 foreach($result as $row)
 {
  $output .= '<option value="'.$row["id"].'">'.$row["name"].'</option>';
 }
 echo $output;


	}


	public function second_level_save(){


			$id = join("','", $this->input->post("selected"));

$query = $this->db->query("SELECT * FROM sub_electives
 WHERE elective_id IN ('".$id."')");
            $result = $query->result_array();
 $output = '';
 foreach($result as $row)
 {
  $output .= '<option value="'.$row["elective_id"].'">'.$row["name"].'</option>';
 }
 echo $output;


	}


	public function change_pp_img()
	{


		 if(!empty($_FILES['myfile']['name'])){
                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $new_name = time().$_FILES["myfile"]['name'];
				$config['file_name'] = $new_name;
                //$config['file_name'] = $_FILES['picture']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('myfile')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                    $data['photo']=$picture;
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }



       $this->db->where('email', $this->session->userdata('userId'));
       $result = $this->db->update('students', $data);

       redirect('student/profile');

	}



	public function manage_profile()
	{
		$data=array();

		//----------------------
		if(!empty($_FILES['resume']['name'])){
                $config['upload_path'] = 'uploads/resume/';
                $config['allowed_types'] = 'pdf|docx|doc|txt';
                $new_name = time().$_FILES["resume"]['name'];
				$config['file_name'] = $new_name;
                //$config['file_name'] = $_FILES['picture']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('resume')){
                    $uploadData = $this->upload->data();
                    $resume = $uploadData['file_name'];
                    $data['resume']=$resume;
                }else{
                    $resume = '';
                }
            }else{
                $resume = '';
            }


            if(!empty($_FILES['picture']['name'])){
                $config['upload_path'] = 'uploads/images/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $new_name = time().$_FILES["picture"]['name'];
				$config['file_name'] = $new_name;
                //$config['file_name'] = $_FILES['picture']['name'];

                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                $this->upload->initialize($config);

                if($this->upload->do_upload('picture')){
                    $uploadData = $this->upload->data();
                    $picture = $uploadData['file_name'];
                    $data['photo']=$picture;
                }else{
                    $picture = '';
                }
            }else{
                $picture = '';
            }
		//----------------------
		$user_email= $this->session->userdata('userId');
		$data['city_display']=$this->input->post('city');
		$data['city']=$this->input->post('city_id');
		$data['mobile']=$this->input->post('mobile');
		$data['objective']=$this->input->post('objective');
		$data['school_city']=$this->input->post('school_city_id');
		$data['school_city_display']=$this->input->post('school_city');
		$school_name = $this->input->post('school_name');
		if($school_name == "other"){
			$my_name = $this->input->post('my_school');
				//----------------------------------
				 $que=$this->db->query("select * from schools where school_name='".$my_name."' and city_id= '".$data['school_city']."'");
            $row = $que->num_rows();
            if($row)
            {
            //$data['error']="<span style='color:red'>This School already exists</span>";
            }
            else
            {
            $que=$this->db->query("INSERT INTO `schools`(`school_name`, `city_id`, `created_by`) VALUES ('$my_name','".$data['school_city']."','$user_email')");

            $school_name = $insert_id = $this->db->insert_id();

            }


				//--------------------------------------
		}

		if($this->input->post('other_skill') != ''){
		    $this->db->select('name');
		    $this->db->where('name',$this->input->post('other_skill'));
		    $query = $this->db->get('skills');
		    if($query->num_rows() > 0){

		    }else{
		        $data = array('name'=>$this->input->post('other_skill'),'status'=>'1');
		        $this->db->insert('skills',$data);
		    }
		}
		$data['school_name']=$school_name;

		//$data['my_school']=$this->input->post('my_school');
		$data['grade']=$this->input->post('grade');
		$data['gpa']=$this->input->post('gpa');
		$data['electives']=$this->input->post('first_level');
		$data['electives'] = implode(', ', $this->input->post('first_level'));
		//$this->input->post('first_level');
		 $data['sub_electives'] = implode(', ', $this->input->post('second_level'));
		//print_r($this->input->post('second_level'));
		//die();
		//print_r($this->input->post('skills'));
	//	$data['skills']= implode(', ', $this->input->post('skills'));
			$data['skills']= $this->input->post('skills');

		$data['links']=$this->input->post('links');
		$data['blog']=$this->input->post('blog');
		$data['work_before']=$this->input->post('work_before');


		$data['insta']=$this->input->post('insta');
		$data['twitter']=$this->input->post('twitter');
		$data['part_prog']=$this->input->post('part_prog');
		//print_r($data); die;
	 $result = $this->Student_model->update_profile($data);
	 if($result){
	 	$this->session->set_flashdata('msg','Profile Updated');
	 	redirect('student/dashboard');
	 }
	}
 public function logout(){
        $this->session->unset_userdata('isStudentLoggedIn');
        $this->session->unset_userdata('userId');
        $this->session->sess_destroy();
        redirect('/');
    }
	public function auth_login(){
        if($this->session->userdata('isStudentLoggedIn')){
            //retunt "true";
             }
                 else{
                     redirect('/');

                 }
     }



     function autocompleteData() {
        $returnData = array();

        // Get skills data
        $conditions['searchTerm'] = $this->input->get('term');
        $conditions['conditions']['status'] = '1';
        $skillData = $this->Student_model->getRows($conditions);

        // Generate array
        if(!empty($skillData)){
            foreach ($skillData as $row){
                $data['id'] = $row['id'];
                $data['value'] = $row['name']."-".$row['sate_name'];
                //$data['state_name'] = $row['sate_name'];
                array_push($returnData, $data);
            }
        }

        // Return results as json encoded array
        echo json_encode($returnData);die;
    }

    function getSchool_list() {
       $returnData = array();
      // $city_id = 6610;
       $city_id = $this->input->post('city_id');
         $schoolData = $this->Student_model->getSchools($city_id);
        // Return results as json encoded array
         if(!empty($schoolData)){
            foreach ($schoolData as $row){
                $data['id'] = $row['id'];
                $data['name'] = $row['school_name'];
                //$data['state_name'] = $row['sate_name'];
                array_push($returnData, $data);
            }

        }
        else{
            	echo "na";
            	die();
            }
        echo json_encode($returnData);die;
    }

    public function get_school_name()
    {
    $id = $this->input->post('id');
      $this->db->where('city',$id);
      $this->db->order_by('name',ASC);
      $query = $this->db->get('tbl_schools');
      //echo $this->db->last_query();
       $res = $query->result();
       $sname = '';
       foreach($res as $res){

           $sname .= '<option value="'.$res->name.'">'.$res->name.'</option>';
       }

       echo $sname;

    }

     public function get_subElec()
    {
    $id = $this->input->post('id');

      $this->db->where_in('elective_id',$id);
      $this->db->order_by('name',ASC);
      $query = $this->db->get('sub_electives');
      //echo $this->db->last_query();
       $res = $query->result();
       $sname = '';
       foreach($res as $res){

           $sname .= '<option value="'.$res->id.'">'.$res->name.'</option>';
       }

       echo $sname;

    }

}
