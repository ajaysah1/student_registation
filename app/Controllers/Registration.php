<?php
namespace App\Controllers;

use App\Models\PersonalModel;
/**
 *
 */
class Registration extends BaseController
{
//
// showPersonalData() {
//   $state = HelperModel.getState();
//   $city = HelperModel.getCity();
//
//   $data(state);
//   $data(city);
//
//   return view('personal_screen');
//
// }
//
// savePersonalDate() {
//   $POST['state'];
//
//   id = RegistrationModel.savePersonalData(a,b,.........);
//
//
// RegistrationModel.saveHighSchoolData(a,b,........., id);
//
//   city = HelperModel.getCityByState(POST['state']);
//   data(id);
//     return view('high_school',$data);
// }
  public function index()
  {
    $model=new PersonalModel();
    $std_data=$model->stateFetch();
    $data=['std_data'=>$std_data,];


  if ($this->request->getMethod()=='post') {
    $rules=[
      'firstname'=>'required|min_length[3]|max_length[20]',
      'lastname'=>'required|min_length[3]|max_length[20]',
      'email'=>'required|min_length[6]|max_length[50]|valid_email|is_unique[student.email_id]',
      'password'=>'required|min_length[8]|max_length[40]',
      'conform_password'=>'matches[password]',
      'phone_num'=>'required|numeric',
      'parent_email'=>'required|min_length[8]|max_length[50]|valid_email|is_unique[student.parent_email]',
    ];
if (! $this->validate($rules)) {
$data['validation']= $this->validator;
}
else {
  $model=new PersonalModel();
  $newData=[
    'first_name'=>$_POST['firstname'],
    'last_name'=>$this->request->getVar('lastname'),
    'email_id'=>$this->request->getVar('email'),
    'password'=>$this->request->getVar('password'),
    'state_id'=>$this->request->getVar('state'),
    'city_id'=>$this->request->getVar('city'),
    'phone_num'=>$this->request->getVar('phone_num'),
    'parent_email'=>$this->request->getVar('parent_email'),
    'insta_handler'=>$this->request->getVar('insta_handle'),
    'website_link'=>$this->request->getVar('website'),
  ];

$student_id=$model->personalRegister($newData);

$session=session();

$session->setFlashdata('success','Sucessful Registartaion');

$_SESSION['student_id']=$student_id;

$get_studen_id=session()->get('student_id');

$stdudent_data=$model->getUserData($get_studen_id);

$_SESSION['data_test']=$stdudent_data;


}

}
  echo view('personal_screen',$data);
  
  }


public function getExtraActivities()
{
$model=new PersonalModel();
$extraActivities=$model->getExtraActivities();
$student_extra_array = array();
foreach($extraActivities as $row) {
  if ( in_array($row['activity_id'], $student_extra_array) ) {
    $selected = "selected ";
  }
  else{
    $selected = "";
  }
  echo '<option '.$selected.'value="'. $row['activity_id'] .'">'.$row['activity_name'].'</option>';
}
}

public function HighSchool()
{
    $model=new PersonalModel();
    $std_data=$model->stateFetch();
    $subject_data=$model->getSubject();
    $data=['std_data'=>$std_data,'subject_data'=>$subject_data];
    if(isset($_POST['subject']))
    {
    $subject=$_POST['subject'];
    $subject_count = count($subject);
    for ($i=0; $i < $subject_count; $i++) {
        $subjectData = array(
          'subject_id'=>$_POST['subject'][$i],
          'student_id'=>$student_id
            );
        $model->subject($subjectData);
        }
      }
$student_id=session()->get('student_id');
if (isset($_POST['school_city'])) {
 $highSchoolData=array(
'city_id'=>$_POST['school_city'],
'skl_name'=>$_POST['school_name'],
'gpa_unweight'=>$_POST['gpa_unweg'],
'gpa_weight'=>$_POST['gpa_wei'],
'student_id'=>$student_id
);
$model->highSchool($highSchoolData);
}

  echo view('high_school',$data);
}


public function getCity() {
  $model=new PersonalModel();
  $id=$this->request->getVar('selectState');
  $citys =$model->getCity($id);

  // echo '<option></option>';
  foreach($citys as $city){
    echo '<option value="'. $city['city_id'] .'">'.$city['city_names'].'</option>';
  }
}
 public function SkillsActivities()
{
$model=new PersonalModel();
  if(isset($_POST['extra_activities']))
  {
  $subject=$_POST['extra_activities'];
  $subject_count = count($subject);
  for ($i=0; $i < $subject_count; $i++) {
      $extraData = array(
        'activity_id'=>$_POST['extra_activities'][$i],
          );
      $model->extraActivity($extraData);
      }
    }



     echo view('skills_achivement');

}

 public function AwardCertificates()
 {

$model=new PersonalModel();
$student_id=session()->get('student_id');
if (isset($_POST['project_details'])) {

$project=$_POST['project_details'];
$project_count=count($project);

for ($i=0; $i<$project_count; $i++) {

$projectData=array (

'project_title'=>$_POST['title'][$i],
'project_description'=>$_POST['project_details'][$i],
'start_date'=>$_POST['start_date'][$i],
'end_date'=>$_POST['end_date'][$i],
'student_id'=>$student_id
);
$model->projectInsert($projectData);
}



}


if (isset($_POST['award'])) {
$award=$_POST['award'];
$award_count=count($award);
for ($i=0; $i <$award_count ; $i++) {
  $awardData=array(
    'award_name'=>$_POST['award'][$i],
    'student_id'=>$student_id
  );
$model->awardInsert($awardData);
}
}

if (isset($_POST['join_date'])) {
$certificate=$_POST['join_date'];
$certificate_count=count($certificate);
for ($i=0; $i <$certificate_count; $i++) {

  $certificateData=array(
    'title'=>$_POST['title'][$i],
    'joined_date'=>$_POST['join_date'][$i],
    'end_date'=>$_POST['expire_date'][$i],
    'student_id'=>$student_id
  );
$model->certificateInsert($certificateData);
}
}

 echo view('awards_certifications');

 }
//
// public function upload()
// {
//   $model=new PersonalModel();
// $file = $this->request->getFile('resume');
// $name = $file->getName();
// $file->move(WRITEPATH.'../assets/upload',$name);
//
// $uploaddata=array(
//
// 'career_goal'=>$_POST['career_goal'],
// 'resume'=>$name,
// );
//
// $model->uploadImage($uploaddata);
//
// }
public function WorkExperience()
 {
   $student_id=session()->get('student_id');
   $model=new PersonalModel();
  if (isset($_POST['start_date'])) {

  $workTitle=$_POST['start_date'];
  $work_count=count($workTitle);
  for ($i=0; $i <$work_count ; $i++) {

  $workData=array (
  'work_title'=>$_POST['title'][$i],
  'comapny_name	'=>$_POST['company_name'][$i],
  'work_location'=>$_POST['location'][$i],
  'start_date	'=>$_POST['start_date'][$i],
  'end_date	'=>$_POST['end_date'][$i],
  'student_id'=>$student_id
  );
  $model->workExperienceInsert($workData);
  }
  $file = $this->request->getFile('resume');
  $name = $file->getName();
  $file->move(WRITEPATH.'../assets/upload',$name);

  $uploaddata=array(

  'career_goal'=>$_POST['career_goal'],
  'resume'=>$name,
  'student_id'=>$student_id
  );

  $model->uploadImage($uploaddata);

}


 echo view('work_experience');
  }




}







 ?>
